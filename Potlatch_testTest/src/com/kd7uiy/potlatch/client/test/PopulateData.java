package com.kd7uiy.potlatch.client.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper.CreateGift;
import com.kd7uiy.potlatch.client.serverApi.MasterApiListing;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;

import android.test.InstrumentationTestCase;

public class PopulateData extends InstrumentationTestCase{
	
	private static final String TAG = "PopulateData";
	
	MasterApiListing api;
	
	@Override
	public void setUp() {
		api=MasterApiListing.getInstance();
	}
	
	public void testLoadData() throws IOException {
		File tempPath=getInstrumentation().getContext().getCacheDir();
		
		//This manages the desert photos
		User user=new User();
		user.setOpenId("desert_lover_87");
		user.setPassword("test");
		user.setName("Cactus");
		
		api.getCombinedService().newUser(user);
		api.authenticate("desert_lover_87","test");
		
		GiftChain cactus=new GiftChain();
		cactus.setName("Cactus");
		cactus.setDescription("For cactus lovers out there");
		cactus=api.getCombinedService().createChain(cactus);
		
		GiftChain sunset=new GiftChain();
		sunset.setName("Sunsets");
		sunset.setDescription("Only for the most spectacular sunsets out there!");
		sunset=api.getCombinedService().createChain(sunset);
		
		api.getCombinedService().addToChain(sunset.getId(), 
				uploadGiftAsset(tempPath,
						"Cactus Mountains Sunset",
						"Sunset comes to the mountains, behind the desert",
						"cactus_mountain_sunset.jpg").getId());
		api.getCombinedService().addToChain(sunset.getId(), 
				uploadGiftAsset(tempPath,
						"Vivid Sunset",
						"See the fiery sun setting behind a cactus, with fire and ice mixing!",
						"cactus_vivid_sunset.jpg").getId());
		api.getCombinedService().addToChain(sunset.getId(), 
				uploadGiftAsset(tempPath,
						"Sunset Silhouette",
						"Imagine the sky bursting before you, with the fiery color of the setting sun!",
						"vivid_sunset.jpg").getId());
		
		api.getCombinedService().addToChain(cactus.getId(), 
				uploadGiftAsset(tempPath,
						"Cactus Cross",
						"See how the lovely cactus blooms, in the shape of a cross!",
						"verde prickly_pear_patch.jpg").getId());
		
		user.setOpenId("animal_lover_84");
		user.setName("Animals are awesome!");
		api.getCombinedService().newUser(user);
		api.authenticate("animal_lover_84","test");
		
		GiftChain animal=new GiftChain();
		animal.setName("Animals");
		animal.setDescription("Awe, isn't he just CUTE!");
		animal=api.getCombinedService().createChain(animal);

		
		api.getCombinedService().addToChain(animal.getId(), 
				uploadGiftAsset(tempPath,
						"Butterfly Kissing Flowers",
						"A beautiful butterfly kisses some bright flowers in spring time.",
						"butterfly_bright_colors.jpg").getId());
		
		api.getCombinedService().addToChain(animal.getId(), 
				uploadGiftAsset(tempPath,
						"Loyal Dog",
						"Aren't dogs just amazing? They sit, and wait on your every move!",
						"loyal_dog.jpg").getId());
		
		api.getCombinedService().addToChain(animal.getId(), 
				uploadGiftAsset(tempPath,
						"Wild Turkey",
						"Wild turkey found in the mountains of Arizona",
						"wild_turkey.jpg").getId());
		
		user.setOpenId("ancient_wonders");
		user.setName("Wonders of the World");
		api.getCombinedService().newUser(user);
		api.authenticate("ancient_wonders","test");
		
		GiftChain wonders=new GiftChain();
		wonders.setName("7 wonders of the world");
		wonders.setDescription("See pictures of the 7 wonders of the world");
		wonders=api.getCombinedService().createChain(wonders);
		
		api.getCombinedService().addToChain(wonders.getId(), 
				uploadGiftAsset(tempPath,
						"Machu Pichu",
						"Matchu Pichu, lost city of the Incas",
						"Machu_Pichu.jpg").getId());

		randomVote(50);
	}
	
	private void randomVote(int numUsers){
		User user=new User();
		user.setOpenId("voter");
		user.setPassword("test");
		user.setName("Just a voter");
		
		Random random=new Random();
		
		List<Gift> gifts=api.getCombinedService().mostTouchingGifts();
		for (int i=0;i<numUsers;i++) {
			String username="voter_"+i;
			user.setOpenId(username);
			api.getCombinedService().newUser(user);
			api.authenticate(username,"test");
			for (Gift gift: gifts) {
				if (random.nextBoolean()) {
					api.getCombinedService().touchGift(gift.getId());
				}
			}
		}
	}

	public Gift uploadGiftAsset(File tempPath, String title, String description, String asset)
			throws IOException, FileNotFoundException {
		Gift gift=new Gift();
		gift.setTitle(title);
		gift.setDescription(description);
		InputStream inStream=getInstrumentation().getContext().getAssets().open(asset);
		
		Gift oGift=api.getAuthenticatedService().createGift(gift);
		CreateGift.uploadImage(oGift, inStream, tempPath);
		return oGift;
	}
}
