package com.kd7uiy.potlatch.client.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import com.kd7uiy.potlatch.client.serverApi.CombinedApi;
import com.kd7uiy.potlatch.client.serverApi.MasterApiListing;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;

import android.graphics.BitmapFactory;
import android.test.InstrumentationTestCase;
import android.util.Log;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class GiftServiceTester extends InstrumentationTestCase{
	
	private static final String TAG = "GiftServiceTester";
	
	MasterApiListing api;
	
	@Override
	public void setUp() {
		Log.v(TAG,"Setting up!");
		
		User user=new User();
		user.setOpenId("test");
		user.setPassword("test");
		user.setName("test");
		user.setPushNotifications(false);
		user.setFilterOffensive(true);
		
		Log.v(TAG,"Setting up user "+user);
		
		api=MasterApiListing.getInstance();
		try {
			user=api.getCombinedService().newUser(user);
		} catch (RetrofitError error) {
			//Ignore, most likely this means the account is created. This is just to make sure we can authenticate.
		}
		api.authenticate("test","test");
		
		Log.v(TAG,"Setup complete!");
	}

	public void testGiftService() throws IOException {

		
		Gift gift=new Gift();
		gift.setTitle("Just a test Gift");
		gift.setDescription("Isn't this Awesome!");
		
//		AssetFileDescriptor descriptor = getContext().getAssets().openFd("ic_launcher.png");
//		FileReader reader = new FileReader(descriptor.getFileDescriptor());
		
//		File file= new File("file:///android_asset/ic_launcher.png");
		
		InputStream inputStream = getInstrumentation().getContext().getAssets().open("ic_launcher.png");
		File file = createFileFromInputStream(inputStream,"ic_launcher");
		
		TypedFile photo=new TypedFile("image/png", file);
		
		
		Gift oGift=api.getAuthenticatedService().createGift(gift);
		api.getAuthenticatedService().uploadGiftImage(oGift.getId(),photo);
		
		Response response=api.getCombinedService().downloadGiftImage(oGift.getId());
		BitmapFactory.decodeStream(response.getBody().in());
		
		List<Gift> gifts=api.getCombinedService().getGifts();
		System.out.println("Gift List");
		for (Gift tgift: gifts) {
			CombinedApi gs=api.getAuthenticatedService();
			gs.touchGift(tgift.getId());
			gs.flagGift(tgift.getId());
			System.out.println(tgift);
			System.out.println("");
			gs.untouchGift(tgift.getId());
			gs.unflagGift(tgift.getId());
		}
	}
	
	public void testMostTouching() throws IOException {
		User user=new User();
		user.setOpenId("test");
		user.setPassword("test");
		user.setName("test");
		user.setPushNotifications(false);
		user.setFilterOffensive(true);
		
		Gift gift=new Gift();
		gift.setTitle("Touching Gift");
		gift.setDescription("Isn't this Awesome!");
		
		InputStream inputStream = getInstrumentation().getContext().getAssets().open("ic_launcher.png");
		File file = createFileFromInputStream(inputStream,"ic_launcher");
		inputStream.close();
		
		TypedFile photo=new TypedFile("image/png", file);
		
		
		for (int i=0;i<10;i++) {
			user.setOpenId("test"+i);
			user.setPassword("test"+i);
			try {
				user=api.getCombinedService().newUser(user);
			} catch (RetrofitError err) {} //Already created
			api.authenticate("test"+i,"test"+i);
			
			gift.setTitle("Aweseome Gift #"+(i+1));
			Gift oGift=api.getAuthenticatedService().createGift(gift);
			api.getAuthenticatedService().uploadGiftImage(oGift.getId(),photo);
		}
		
		for (int i=0;i<10;i++) {
			api.authenticate("test"+i,"test"+i);
			
			for (int j=i+1;j<11;j++) {
				api.getAuthenticatedService().touchGift(j);
			}
			
			api.getCombinedService().mostTouchingGifts();
			api.getCombinedService().getGiftTouchedUsers(1);
		}
	}
	
	public void testGiftChain() throws IOException {
		Gift gift=new Gift();
		gift.setTitle("Some Gift");
		gift.setDescription("Isn't this Awesome!");
		
		InputStream inputStream = getInstrumentation().getContext().getAssets().open("ic_launcher.png");
		File file = createFileFromInputStream(inputStream,"ic_launcher");
		inputStream.close();
		
		TypedFile photo=new TypedFile("image/png", file);
		
		GiftChain chain=new GiftChain();
		chain.setName("Awesome Stuff!");
		
		chain=api.getAuthenticatedService().createChain(chain);
		
		
		for (int i=0;i<10;i++) {
			gift.setTitle("Awesome Gift #"+(i+1));
			Gift oGift=api.getAuthenticatedService().createGift(gift);
			api.getAuthenticatedService().uploadGiftImage(oGift.getId(),photo);
			
			api.getAuthenticatedService().addToChain(chain.getId(),oGift.getId());
		}
		
		Response response=api.getCombinedService().downloadGiftImage(chain.getId());
		BitmapFactory.decodeStream(response.getBody().in());
		
		List<GiftChain> chains=api.getCombinedService().getChains();
		
		Log.v(TAG,Arrays.toString(chains.toArray(new GiftChain[]{})));
		
	}

	private File createFileFromInputStream(InputStream inputStream,String outputName) {

	   try{
	      File f = File.createTempFile(outputName,".png");
	      OutputStream outputStream = new FileOutputStream(f);
	      byte buffer[] = new byte[1024];
	      int length = 0;

	      while((length=inputStream.read(buffer)) > 0) {
	        outputStream.write(buffer,0,length);
	      }

	      outputStream.close();
	      inputStream.close();

	      return f;
	   }catch (IOException e) {
	         //Logging exception
	   }

	return null;
	}

}
