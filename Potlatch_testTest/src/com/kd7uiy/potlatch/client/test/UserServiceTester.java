package com.kd7uiy.potlatch.client.test;

import com.kd7uiy.potlatch.client.serverApi.MasterApiListing;
import com.kd7uiy.potlatch.shared.User;

import retrofit.RetrofitError;
import junit.framework.TestCase;

public class UserServiceTester extends TestCase{
	
	@SuppressWarnings("unused")
	private static final String TAG = "UserServiceTester";

	public void testUserService() {
		User user=new User();
		user.setOpenId("kd7uiy");
		user.setPassword("test");
		user.setName("test");
		user.setPushNotifications(false);
		user.setFilterOffensive(true);
		
		MasterApiListing api=MasterApiListing.getInstance();
		boolean pass=false;
		try {
			user=api.getCombinedService().newUser(user);
		} catch (RetrofitError ex) {
		}
		try {
			user=api.getCombinedService().newUser(user);
		} catch (RetrofitError ex) {
			pass=true;
		}
		assertTrue("Should have thrown a 406 to indicate a duplicate username",pass);
		api.authenticate("kd7uiy","test");
		user.setPushNotifications(true);
		User updatedUser=api.getCombinedService().updateUser(user);
		assertTrue(updatedUser.getId()==user.getId());
		assertTrue(updatedUser.getPushNotifications()==true);
		User currentUser=api.getCombinedService().getCurrentUser();
		assertTrue(currentUser.getId()==updatedUser.getId());
	}
	
	public void testSwitchUser() {
		User user=new User();
		user.setOpenId("user1");
		user.setPassword("test");
		user.setName("test");
		user.setPushNotifications(false);
		user.setFilterOffensive(true);
		
		MasterApiListing api=MasterApiListing.getInstance();
		try {
			user=api.getCombinedService().newUser(user);
		} catch (RetrofitError ex) {
		}
		api.authenticate("user1","test");
		User cUser=api.getAuthenticatedService().getCurrentUser();
		assertTrue("Usernames do not match",cUser.getId()==user.getId());
		
		user.setOpenId("user2");
		try {
			user=api.getCombinedService().newUser(user);
		} catch (RetrofitError ex) {
		}
		api.authenticate("user2","test");
		cUser=api.getAuthenticatedService().getCurrentUser();
		assertTrue("Usernames do not match",cUser.getId()==user.getId());
	}

}
