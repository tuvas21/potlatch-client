package com.kd7uiy.library;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;

/**
 * @class DownloadUtils
 *
 * @brief This class encapsulates several static methods so that all
 *        Services can access them without redefining them in each
 *        Service.
 */
public class CommandUtils {
	/**
     * Used for debugging.
     */
    static final String TAG = "MethodUtils";
    
    public static final String MESSENGER = "MESSENGER";
	protected static final String COMMAND = "Command";

	public static final String WHAT = "WHAT";

	public static final int DEFAULT_MSG_ID = 0;

	protected static final int ERROR_MSG_ID = -1;

	public static final String EXTRAS = "bundle-extras";
	
    /**
     * Make an Intent which will start a service if provided as a
     * parameter to startService().
     * 
     * @param context		The context of the calling component
     * @param service		The class of the service to be
     *                          started. (For example, ThreadPoolDownloadService.class) 
     * @param handler		The handler that the service should
     *                          use to return a result. 
     * @param uri		The web URL that the service should download
     * @param what 		A what value passed with the message created.
     * @param extras		Bundle of extra items to pack with the Intent
     * 
     * This method is an example of the Factory Method Pattern,
     * because it creates and returns a different Intent depending on
     * the parameters provided to it.
     * 
     * The Intent is used as the Command Request in the Command
     * Processor Pattern when it is passed to the
     * ThreadPoolDownloadService using startService().
     * 
     * The Handler is used as the Proxy, Future, and Servant in the
     * Active Object Pattern when it is passed a message, which serves
     * as the Active Object, and acts depending on what the message
     * contains.
     * 
     * The handler *must* be wrapped in a Messenger to allow it to
     * operate across process boundaries.
     * 
     */
    public static Intent makeMessengerIntent(Context context,
                                             Class<?> service,
                                             Handler handler,
                                             int what, 
                                             Command command,
                                             Bundle extras) {
    	Messenger messenger = new Messenger(handler);
    	
    	Intent intent = new Intent(context,
                                   service);
    	intent.putExtra(MESSENGER,messenger);
    	intent.putExtra(COMMAND,command);
    	intent.putExtra(WHAT,what);
    	intent.putExtra(EXTRAS,extras);
    	
        return intent;
    }

}
