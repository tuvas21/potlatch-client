package com.kd7uiy.library;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public abstract class ZoomViewAdapter<T extends Parcelable> extends StateSavingArrayAdapter<T> implements OnItemClickListener{

	@SuppressWarnings("unused")
	private static final String TAG = "GiftViewAdapter";
	protected Context mContext;
	protected static final long SHORT_ANIMATION_DURATION = 200;
	private View mParentView;
	private boolean mCanZoomInOnImage = true;
	protected AnimatorSet mCurrentAnimator;
	private float currentElevation;
	private float maxElevation;
	
	public ZoomViewAdapter(Context context) {
		super();
		mContext=context;
	}
	
	protected abstract CardView populateImage(View parent,int position);

	private void zoomImageFromThumb(final View parent, final CardView thumbView, int position) {
		mCanZoomInOnImage=false;
	    // If there's an animation in progress, cancel it
	    // immediately and proceed with this one.
	    if (mCurrentAnimator != null) {
	        mCurrentAnimator.cancel();
	    }
	    
	    final CardView expandedImageView=populateImage(parent,position);
	    
	    currentElevation = thumbView.getCardElevation();
	    maxElevation=expandedImageView.getMaxCardElevation();
	    
	    // Calculate the starting and ending bounds for the zoomed-in image.
	    // This step involves lots of math. Yay, math.
	    final Rect startBounds = new Rect();
	    final Rect finalBounds = new Rect();
	    final Point globalOffset = new Point();
	
	    // The start bounds are the global visible rectangle of the thumbnail,
	    // and the final bounds are the global visible rectangle of the container
	    // view. Also set the container view's offset as the origin for the
	    // bounds, since that's the origin for the positioning animation
	    // properties (X, Y).
	    thumbView.getGlobalVisibleRect(startBounds);
	    parent.getGlobalVisibleRect(finalBounds, globalOffset);
	    startBounds.offset(-globalOffset.x, -globalOffset.y);
	    finalBounds.offset(-globalOffset.x, -globalOffset.y);
	
	    // Adjust the start bounds to be the same aspect ratio as the final
	    // bounds using the "center crop" technique. This prevents undesirable
	    // stretching during the animation. Also calculate the start scaling
	    // factor (the end scaling factor is always 1.0).
	    float startScale;
	    if ((float) finalBounds.width() / finalBounds.height()
	            > (float) startBounds.width() / startBounds.height()) {
	        // Extend start bounds horizontally
	        startScale = (float) startBounds.height() / finalBounds.height();
	        float startWidth = startScale * finalBounds.width();
	        float deltaWidth = (startWidth - startBounds.width()) / 2;
	        startBounds.left -= deltaWidth;
	        startBounds.right += deltaWidth;
	    } else {
	        // Extend start bounds vertically
	        startScale = (float) startBounds.width() / finalBounds.width();
	        float startHeight = startScale * finalBounds.height();
	        float deltaHeight = (startHeight - startBounds.height()) / 2;
	        startBounds.top -= deltaHeight;
	        startBounds.bottom += deltaHeight;
	    }
	
	    // Hide the thumbnail and show the zoomed-in view. When the animation
	    // begins, it will position the zoomed-in view in the place of the
	    // thumbnail.
	    thumbView.setAlpha(0f);
	    expandedImageView.setVisibility(View.VISIBLE);
	
	    // Set the pivot point for SCALE_X and SCALE_Y transformations
	    // to the top-left corner of the zoomed-in view (the default
	    // is the center of the view).
	    expandedImageView.setPivotX(0f);
	    expandedImageView.setPivotY(0f);
	
	    // Construct and run the parallel animation of the four translation and
	    // scale properties (X, Y, SCALE_X, and SCALE_Y).
	    AnimatorSet set = new AnimatorSet();
	    set
	            .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
	                    startBounds.left, finalBounds.left))
	            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
	                    startBounds.top, finalBounds.top))
	            .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
	            		startScale, 1f))
	            .with(ObjectAnimator.ofFloat(expandedImageView,
	                    View.SCALE_Y, startScale, 1f))
	            .with(ObjectAnimator.ofFloat(expandedImageView,
			            "cardElevation",currentElevation,maxElevation));;
	    set.setDuration(SHORT_ANIMATION_DURATION);
	    set.setInterpolator(new DecelerateInterpolator());
	    set.addListener(new AnimatorListenerAdapter() {
	        @Override
	        public void onAnimationEnd(Animator animation) {
	            mCurrentAnimator = null;
	        }
	
	        @Override
	        public void onAnimationCancel(Animator animation) {
	            mCurrentAnimator = null;
	        }
	    });
	    set.start();
	    mCurrentAnimator = set;
	
	    // Upon clicking the zoomed-in image, it should zoom back down
	    // to the original bounds and show the thumbnail instead of
	    // the expanded image.
	    final float startScaleFinal = startScale;
	    expandedImageView.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
	            zoomOutOfImage(thumbView, expandedImageView, startBounds,
						startScaleFinal);
	        }
	    });
	
	}

	private void zoomOutOfImage(final CardView thumbView, final CardView expandedImageView, final Rect startBounds,
			final float startScaleFinal) {
				if (mCurrentAnimator != null) {
			        mCurrentAnimator.cancel();
			    }
			
			    // Animate the four positioning/sizing properties in parallel,
			    // back to their original values.
			    AnimatorSet set = new AnimatorSet();
			    set.play(ObjectAnimator
			                .ofFloat(expandedImageView, View.X, startBounds.left))
			                .with(ObjectAnimator
			                        .ofFloat(expandedImageView, 
			                                View.Y,startBounds.top))
			                .with(ObjectAnimator
			                        .ofFloat(expandedImageView, 
			                                View.SCALE_X, startScaleFinal))
			                .with(ObjectAnimator
			                        .ofFloat(expandedImageView, 
			                                View.SCALE_Y, startScaleFinal))
			                .with(ObjectAnimator
			                		.ofFloat(expandedImageView,
			                				"cardElevation",maxElevation,currentElevation));
			    set.setDuration(SHORT_ANIMATION_DURATION);
			    set.setInterpolator(new DecelerateInterpolator());
			    set.addListener(new AnimatorListenerAdapter() {
			        @Override
			        public void onAnimationEnd(Animator animation) {
			            thumbView.setAlpha(1f);
			            expandedImageView.setVisibility(View.GONE);
			            mCurrentAnimator = null;
			        }
			
			        @Override
			        public void onAnimationCancel(Animator animation) {
			            thumbView.setAlpha(1f);
			            expandedImageView.setVisibility(View.GONE);
			            mCurrentAnimator = null;
			        }
			    });
			    set.start();
			    mCurrentAnimator = set;
			    mCanZoomInOnImage=true;
			}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
				if (mCanZoomInOnImage) {
					zoomImageFromThumb((View) parent.getParent().getParent(),(CardView)view,position);
				//	Log.v(TAG,"Zooming in on image "+position);
				}
				
			}

	public View getParentView() {
		return mParentView;
	}

	public void setParentView(View mParentView) {
		this.mParentView = mParentView;
	}
}