package com.kd7uiy.library;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

/**
 * <p>
 * An {@code ArrayAdapter} that also knows how to save and restore its state.
 * Basically all it does is save/restore the array of objects being managed by
 * the Adapter.
 * </p>
 * 
 * <p>
 * Note that this only saves the items and not things like checked item
 * positions. Those belong to the {@link ListView} itself. Consider using
 * {@link ListView#onSaveInstanceState()} and
 * {@link ListView#onRestoreInstanceState(Parcelable)} for this purpose.
 * </p>
 * 
 * 
 * @author Kiran Rao
 * @url https://gist.github.com/curioustechizen/6ed0981b013f63236f0b
 * 
 * @param <T>
 *            The type of objects managed by this Adapter. Note that it must be
 *            a Parcelable.
 */
public abstract class StateSavingArrayAdapter<T extends Parcelable> extends
		BaseAdapter {

	ArrayList<T> mArray = new ArrayList<>();

	private static final String KEY_ADAPTER_STATE = "StateSavingArrayAdapter.KEY_ADAPTER_STATE";

	/**
	 * Saves the instance state of this {@link ArrayAdapter}. It saves the array
	 * of currently managed by this adapter
	 * 
	 * @param outState
	 *            The bundle into which the state is saved
	 */
	public void onSaveInstanceState(Bundle outState) {
		outState.putParcelableArrayList(KEY_ADAPTER_STATE, getAllItems());
	}

	/**
	 * Restore the instance state of the {@link ArrayAdapter}. It re-initializes
	 * the array of objects being managed by this adapter with the state
	 * retrieved from {@code savedInstanceState}
	 * 
	 * @param savedInstanceState
	 *            The bundle containing the previously saved state
	 */
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		if (savedInstanceState.containsKey(KEY_ADAPTER_STATE)) {
			ArrayList<T> objects = savedInstanceState
					.getParcelableArrayList(KEY_ADAPTER_STATE);
			setItems(objects);
		}
	}
	
	/**
	 * Adds an item to the array
	 */
	public void add(int pos,T item) {
		mArray.add(pos,item);
	}
	
	/**
	 * Adds an item to the array
	 */
	public void add(T item) {
		mArray.add(item);
	}

	/**
	 * Gets all the items in this adapter as an {@code ArrayList}
	 */
	public ArrayList<T> getAllItems() {
		return mArray;
	}

	/*
	 * Replaces the items in the adapter with those in this list
	 * 
	 * @param items The items to set into the adapter.
	 */
	public void setItems(ArrayList<T> items) {
		mArray.clear();
		mArray.addAll(items);
	}

	@Override
	public int getCount() {
		return mArray.size();
	}

	@Override
	public Object getItem(int position) {
		return mArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void clear() {
		mArray.clear();
	}

	public void remove(int pos) {
		mArray.remove(pos);
	}
}
