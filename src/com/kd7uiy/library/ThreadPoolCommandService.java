package com.kd7uiy.library;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

/**
 * @class ThreadPoolDownloadService
 *
 * @brief This Service handles downloading several files concurrently
 *        within a pool of Threads.  When it is created, it creates a
 *        ThreadPoolExecutor using the newFixedThreadPool() method of
 *        the Executors class.
 * 
 *        When this Service is started, it should be supplied with a
 *        URI for download and a Messenger.  It downloads the URI
 *        supplied, stores it on the Android file system, then returns
 *        the pathname of the downloaded file using the supplied
 *        Messenger.
 * 
 *        This class implements the Synchronous Service layer of the
 *        Half-Sync/Half-Async pattern.  It also implements a variant
 *        of the Factory Method pattern.
 */
public class ThreadPoolCommandService extends Service {
    /**
     * A class constant that determines the maximum number of threads
     * used to service download requests.
     */
    static final int MAX_THREADS = 4;

	protected static final String TAG = "TPDS";
	
    /**
     * The ExecutorService that references a ThreadPool.
     */
    ExecutorService mExecutor;

    /**
     * Hook method called when the Service is created.
     */
    public void onCreate() {
        // TODO - You fill in here to replace null with a new
        // FixedThreadPool Executor that's configured to use
        // MAX_THREADS. Use a factory method in the Executors class.

        mExecutor = Executors.newFixedThreadPool(MAX_THREADS);
    }

    /**
     * Make an intent that will start this service if supplied to
     * startService() as a parameter.
     * 
     * @param context		The context of the calling component.
     * @param handler		The handler that the service should
     *                          use to respond with a result  
     * @param uri               The web URL of a file to download
     * @param what			int code passed back with the message
     * @param extras		Bundle of extra items to pack with the Intent
     * 
     * This method utilizes the Factory Method makeMessengerIntent()
     * from the DownloadUtils class.  The returned intent is a Command
     * in the Command Processor Pattern. The intent contains a
     * messenger, which plays the role of Proxy in the Active Object
     * Pattern.
     */
    public static Intent makeIntent(Context context,
                                    Handler handler,
                                    int what,
                                    Command command,
                                    Bundle extras) {
        return CommandUtils.makeMessengerIntent(context,ThreadPoolCommandService.class,handler,what,command,extras);
    }

    /**
     * Hook method called when a component calls startService() with
     * the proper Intent.
     */
    @Override
    public int onStartCommand(final Intent intent,
                              int flags,
                              int startId) {

        Runnable downloadRunnable = new Runnable() {
			@Override
			public void run() {
				intent.setExtrasClassLoader(getClass().getClassLoader());
				Command command=(Command) intent.getParcelableExtra(CommandUtils.COMMAND);
				Messenger messenger=intent.getParcelableExtra(CommandUtils.MESSENGER);
		    	Message message=Message.obtain();
		    	Bundle bundle=command.execute();
		    	if (bundle==null) {
		    		message.what=CommandUtils.ERROR_MSG_ID;
		    	} else {
		    		message.what=intent.getIntExtra(CommandUtils.WHAT,CommandUtils.DEFAULT_MSG_ID);
		    		message.setData(bundle);
		    	}
		    	try {
					messenger.send(message);
				} catch (RemoteException e) {
//					e.printStackTrace();
				}
//		    	message.recycle();
			}
        };

        mExecutor.execute(downloadRunnable);
      
        // Tell the Android framework how to behave if this service is
        // interrupted.  In our case, we want to restart the service
        // then re-deliver the intent so that all files are eventually
        // downloaded.
        return START_REDELIVER_INTENT;
    }

    /**
     * Called when the service is destroyed, which is the last call
     * the Service receives informing it to clean up any resources it
     * holds.
     */
    public void onDestroy() {
    	// Ensure that the threads used by the ThreadPoolExecutor
    	// complete and are reclaimed by the system.

        mExecutor.shutdown();
    }

    /**
     * Return null since this class does not implement a Bound
     * Service.
     */
    public IBinder onBind (Intent intent) {
        return null;
    }
}
