package com.kd7uiy.potlatch.client.dialog;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.adapters.GiftChainViewAdapter;
import com.kd7uiy.potlatch.client.adapters.GiftChainViewAdapter.AdapterType;
import com.kd7uiy.potlatch.client.serverApi.GiftChainCommandWrapper.AddToChain;
import com.kd7uiy.potlatch.controller.UserManager;
import com.kd7uiy.potlatch.shared.Gift;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class GiftChainChooseFragment extends DialogFragment implements OnItemClickListener{

	
	private static final String GIFT = "gift";
	private static final String TAG = "chain";
	private TextView mGiftTitle;
	private GridView mGiftChainGridView;
	private Gift mGift;
	private GiftChainViewAdapter mGiftChainAdapter;

	//Pass in the gift post create;
	public static GiftChainChooseFragment getInstance(Gift gift) {
		Bundle bundle=new Bundle();
		bundle.putParcelable(GIFT,gift);
		GiftChainChooseFragment frag=new GiftChainChooseFragment();
		frag.setArguments(bundle);
		return frag;
	}
	
	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog( Bundle savedInstanceState) {
		LayoutInflater inflater=LayoutInflater.from(getActivity());
		View view=inflater.inflate(R.layout.dialog_gift_chain_chooser,null);
		
		mGift=(Gift)getArguments().getParcelable(GIFT);
		
		mGiftChainGridView=(GridView) view.findViewById(R.id.gridview);
		mGiftTitle=(TextView) view.findViewById(R.id.gift_title);
		
		mGiftTitle.setText(mGift.getTitle());
		
		mGiftChainAdapter=new GiftChainViewAdapter(getActivity(), AdapterType.GIFT_CHAIN_SMALL,UserManager.getInstance().getGiftChains());
		mGiftChainGridView.setAdapter(mGiftChainAdapter);
		
		mGiftChainGridView.setOnItemClickListener(this);
		

		return new AlertDialog.Builder(getActivity())
			.setTitle(R.string.add_to_gift_chain_)
			.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
            )
            .setView(view)
            .create();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (id==GiftChainViewAdapter.NEW_CHAIN) {
			GiftChainViewAdapter.newGiftChain(mGift,getActivity());
		} else {
			Intent intent=ThreadPoolCommandService.makeIntent(getActivity(), new CommandHandler(getActivity()),
					CommandHandler.CHAIN_INFO_UPDATE,
					new AddToChain(id, mGift.getId()), null);
			getActivity().startService(intent);
		}
		dismiss();
	}
}
