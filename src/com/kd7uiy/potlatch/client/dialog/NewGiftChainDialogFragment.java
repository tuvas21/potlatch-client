package com.kd7uiy.potlatch.client.dialog;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.serverApi.GiftChainCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.GiftChainCommandWrapper.NewChain;
import com.kd7uiy.potlatch.controller.UserManager;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class NewGiftChainDialogFragment extends DialogFragment {
	@SuppressWarnings("unused")
	private static final String TAG = "NewAccout";
	private static final String GIFT = "gift";
	View mView=null;
	private TextView mNameView;
	private TextView mDescriptionView;
	private Gift mGift;
	
	public static NewGiftChainDialogFragment getInstance(Gift gift) {
		NewGiftChainDialogFragment frag=new NewGiftChainDialogFragment();
		Bundle bundle=new Bundle();
		bundle.putParcelable(GIFT,gift);
		frag.setArguments(bundle);
		
		if (UserManager.getInstance().getUser()==null) {
			return null;
		}
		return frag;
	}
	
	private NewGiftChainDialogFragment() {
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog( Bundle savedInstanceState) {
		
		Bundle bundle=getArguments();
		mGift=bundle.getParcelable(GIFT);
		
		LayoutInflater inflater=LayoutInflater.from(getActivity());
		mView=inflater.inflate(R.layout.dialog_new_gift_chain,null);
		
		mNameView=(TextView) mView.findViewById(R.id.name);
		mDescriptionView=(TextView) mView.findViewById(R.id.description);

		
		return new AlertDialog.Builder(getActivity())
			.setTitle(R.string.create_new_gift_chain)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    sendUpdateToServer();
                }
			})
			.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
            )
            .setView(mView)
            .create();
	}

	protected void sendUpdateToServer() {
		GiftChain chain=new GiftChain();
		chain.setName(mNameView.getText().toString());
		chain.setDescription(mDescriptionView.getText().toString());

		Log.v(TAG,"Creating chain "+chain);
		
		try {
			Intent intent=null;
			if (mGift==null) {
				intent=ThreadPoolCommandService.makeIntent(getActivity(),
					new CommandHandler(getActivity()),
					CommandHandler.NEW_GIFT_CHAIN,
					new NewChain(chain), null);
			} else {
				intent=ThreadPoolCommandService.makeIntent(getActivity(),
						new CommandHandler(getActivity()),
						CommandHandler.NEW_GIFT_CHAIN,
						new GiftChainCommandWrapper.NewChainWithGift(chain,mGift), null);
			}
			getActivity().startService(intent);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
