package com.kd7uiy.potlatch.client.dialog;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.serverApi.UserCommandWrapper;
import com.kd7uiy.potlatch.shared.User;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class NewAccountDialogFragment extends DialogFragment {
	
	@SuppressWarnings("unused")
	private static final String TAG = "NewAccout";
	private View mView=null;
	private TextView mUsernameView;
	private TextView mPasswordView;
	private TextView mShownNameView;
	private CheckBox mPushNotices;
	private CheckBox mBlockOffensive;
	private TextView mPasswordConfirmView;
	
	private class EqualTextWatcher implements TextWatcher {
		private TextView mCompare;
		private TextView mOriginal;
		private String mNotEqual;

		private EqualTextWatcher(TextView original, TextView compare) {
			mOriginal=original;
			mCompare=compare;
			mNotEqual=original.getContext().getString(R.string.passwords_not_equal);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,int count) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (mOriginal.getText().toString().equals(mCompare.getText().toString())) {
				mCompare.setError(null);
			} else {
				
				mCompare.setError(mNotEqual);
			}
		}
		
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog( Bundle savedInstanceState) {
		LayoutInflater inflater=LayoutInflater.from(getActivity());
		mView=inflater.inflate(R.layout.dialog_new_user,null);
		
		mUsernameView=(TextView) mView.findViewById(R.id.username);
		mPasswordView=(TextView) mView.findViewById(R.id.password);
		mPasswordConfirmView=(TextView) mView.findViewById(R.id.password_confirm);
		EqualTextWatcher equalWatcher=new EqualTextWatcher(mPasswordView,mPasswordConfirmView);
		mPasswordView.addTextChangedListener(equalWatcher);
		mPasswordConfirmView.addTextChangedListener(equalWatcher);
		mShownNameView=(TextView) mView.findViewById(R.id.person_name);
		mPushNotices=(CheckBox) mView.findViewById(R.id.push_notices);
//		mBlockOffensive=(CheckBox) mView.findViewById(R.id.block_offensive);
		
		return new AlertDialog.Builder(getActivity())
			.setTitle(R.string.create_new_user)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    sendUpdateToServer();
                }
			})
			.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
            )
            .setView(mView)
            .create();
	}

	//TODO Put in validation that the two user names are correct.
	protected void sendUpdateToServer() {
		User user=new User();
		user.setOpenId(mUsernameView.getText().toString());
		user.setPassword(mPasswordView.getText().toString());
		user.setName(mShownNameView.getText().toString());
		user.setPushNotifications(mPushNotices.isChecked());
//		user.setFilterOffensive(mBlockOffensive.isChecked());
		
		try {
			Intent intent=ThreadPoolCommandService.makeIntent(getActivity(),
					new CommandHandler(getActivity()),
					CommandHandler.NEW_USER_CREATED,
					new UserCommandWrapper.NewUser(user), null);
			getActivity().startService(intent);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
