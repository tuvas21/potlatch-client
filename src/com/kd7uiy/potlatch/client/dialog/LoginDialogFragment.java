package com.kd7uiy.potlatch.client.dialog;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.serverApi.MasterApiListing;
import com.kd7uiy.potlatch.client.serverApi.UserCommandWrapper;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class LoginDialogFragment extends DialogFragment {
	
	@SuppressWarnings("unused")
	private static final String TAG = "NewAccout";
	View mView=null;
	TextView mUsernameView;
	TextView mPasswordView;
	TextView mShownNameView;
	CheckBox mPushNotices;
	CheckBox mBlockOffensive;

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog( Bundle savedInstanceState) {
		LayoutInflater inflater=LayoutInflater.from(getActivity());
		mView=inflater.inflate(R.layout.dialog_login_user,null);
		
		mUsernameView=(TextView) mView.findViewById(R.id.username);
		mPasswordView=(TextView) mView.findViewById(R.id.password);
		
		SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(getActivity());
		mUsernameView.setText(prefs.getString("username",""));
		mPasswordView.setText(prefs.getString("password",""));
		
		return new AlertDialog.Builder(getActivity())
			.setTitle(R.string.log_in)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    loginToServer();
                }
			})
			.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
            )
            .setView(mView)
            .create();
	}

	protected void loginToServer() {
		String username=mUsernameView.getText().toString();
		String password=mPasswordView.getText().toString();
		MasterApiListing.getInstance().authenticate(username,password);
		
		Intent intent=ThreadPoolCommandService.makeIntent(getActivity(), new CommandHandler(getActivity()),
				CommandHandler.LOGGED_IN_USER,
				new UserCommandWrapper.CurrentUser(), null);
		getActivity().startService(intent);
	}
}
