package com.kd7uiy.potlatch.client.serverApi;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.potlatch.client.serverApi.external.EasyHttpClient;
import com.kd7uiy.potlatch.client.serverApi.external.SecuredRestBuilder;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

public class MasterApiListing {
	
//	private static final String URL = "https://10.0.3.2:8443";
	private static final String URL = "https://192.168.1.5:8443";
	private static final String WRITER_CLIENT_ID = "Android";
	//Might use this someday
	@SuppressWarnings("unused")
	private static final String READER_CLIENT_ID="mobileReader";
	@SuppressWarnings("unused")
	private static final String TAG = "mAPI";
	
	private boolean isUserAuthenticated=false;
	private CombinedApi combinedService=null;
	private volatile static MasterApiListing instance = new MasterApiListing();
	
	public static synchronized MasterApiListing getInstance() {
		return instance;
	}

	private MasterApiListing() {
		combinedService=new RestAdapter.Builder()
			.setClient(new ApacheClient(new EasyHttpClient()))
			.setLogLevel(LogLevel.NONE)
			.setEndpoint(URL).build().create(CombinedApi.class);
	}
	
	public void authenticate(String username, String password) {
		combinedService = new SecuredRestBuilder()
			.setClient(new ApacheClient(new EasyHttpClient()))
			.setEndpoint(URL)
			.setLoginEndpoint(URL + CombinedApi.TOKEN_PATH)
			.setLogLevel(LogLevel.NONE)
			.setUsername(username).setPassword(password)
			.setClientId(WRITER_CLIENT_ID)
			.build().create(CombinedApi.class);
		
		isUserAuthenticated=true;
	}
	
	public CombinedApi getCombinedService() {
		return combinedService;
	}
	
	public CombinedApi getAuthenticatedService() {
		if (isUserAuthenticated) {
			return combinedService;
		} else {
			throw new IllegalStateException("User is not authenticated!");
		}
	}

	
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("this="+System.identityHashCode(this));
		sb.append(" isUserAuthenticated="+isUserAuthenticated);
		return sb.toString();
	}

}
