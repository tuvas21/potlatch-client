package com.kd7uiy.potlatch.client.serverApi;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import com.kd7uiy.library.Command;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.shared.Gift;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class GiftCommandWrapper {
	
	private static final String TAG="GiftCommand";
	
	public static final String GIFT = "gift";
	public static final String BITMAP = "bitmap";
	public static final String GIFT_INFO = "gift-info";
	public static final String GIFT_ID = "gift_id";

	public enum VoteType {TOUCHED, UNTOUCHED, FLAGGED, UNFLAGGED};
	
	public static abstract class GiftCommand extends Command {
		protected Gift mGift;
		protected String mImagePath;

		public GiftCommand(Gift gift, String imagePath) {
			mGift=gift;
			mImagePath=imagePath;
		}
		
		public GiftCommand(Parcel parcel) {
			super(parcel);
			mGift=parcel.readParcelable(Gift.class.getClassLoader());
			mImagePath=parcel.readString();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(mGift,flags);
			dest.writeString(mImagePath);
		}
		

		@Override
		public Bundle execute() {
			// Does nothing, has to be non-abstract to make this work correctly...
			return null;
		}
	}

	public static class CreateGift extends GiftCommand {

		private static final int MAX_WIDTH = 1024;
		private File mTempPath;

		public CreateGift(Gift gift, String imagePath,File tempPath) {
			super(gift, imagePath);
			mTempPath=tempPath;
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			Gift gift=MasterApiListing.getInstance().getCombinedService().createGift(mGift);
			try {
				uploadImage(gift, new FileInputStream(mImagePath),mTempPath);
				
				bundle.putParcelable(GIFT,gift);
			} catch (IOException ex) {
			}
			return bundle;
		}

		public static void uploadImage(Gift gift, InputStream file, File tempPath) throws IOException,
				FileNotFoundException {
			Bitmap bit=BitmapFactory.decodeStream(file);
			int newWidth=bit.getHeight();
			int newHeight=bit.getWidth();
			if (bit.getHeight()>MAX_WIDTH) {
				newWidth=MAX_WIDTH;
				newHeight=bit.getHeight()/(int)((double)bit.getWidth()/MAX_WIDTH);
			}
			Bitmap scaled=Bitmap.createScaledBitmap(bit,newWidth,newHeight,true);
			File outputFile = File.createTempFile("prefix", "extension", tempPath);
			OutputStream oStream=new FileOutputStream(outputFile);
			scaled.compress(Bitmap.CompressFormat.PNG,100,oStream);
			bit.recycle();
			scaled.recycle();
			oStream.close();
			
			TypedFile photo=new TypedFile("image/png", outputFile);
			
			MasterApiListing.getInstance().getCombinedService().uploadGiftImage(gift.getId(),photo);
			
			outputFile.delete();
		}
		
		public CreateGift(Parcel parcel) {
			super(parcel);
			mTempPath=(File) parcel.readSerializable();
		}
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest,flags);
			dest.writeSerializable(mTempPath);
		}
		
		@Override
		public int describeContents() {
			return 1006;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new CreateGift(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class DownloadGift extends Command {

		private long mId;

		public DownloadGift(long id) {
			mId=id;
		}
		public DownloadGift(Gift gift) {
			mId=gift.getId();
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			try { 
				Bitmap bm;
				try {
					Response response=MasterApiListing.getInstance().
							getCombinedService().
							downloadGiftImage(mId);
					bm=BitmapFactory.decodeStream(response.getBody().in());
				} catch (RetrofitError ex) {
					bm=null;
				}
				bundle.putLong(GIFT_ID,mId);
				bundle.putParcelable(BITMAP,bm);
			} catch (IOException ex) {
				return null;
			}
			return bundle;
		}
		
		public DownloadGift(Parcel parcel) {
			mId=parcel.readLong();
		}
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(mId);
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new DownloadGift(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class GiftInfo extends GiftCommand {
		
		long[] mIds;

		public GiftInfo(long[] giftIds) {
			super(null, null);
			mIds=giftIds;
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			ArrayList<Gift> gifts=new ArrayList<>();
			for (long giftId: mIds) {
				gifts.add(MasterApiListing.getInstance().getCombinedService().getGiftInfo(giftId));
			}
			Log.v(TAG,"Returning execute");
			bundle.putParcelableArrayList(GIFT_INFO,gifts);
			bundle.putParcelable(CommandHandler.COMMAND,this);
			return bundle;
		}
		
		@Override
		public int describeContents() {
			return 1002;
		}
		
		public GiftInfo(Parcel parcel) {
			super(parcel);
			mIds=parcel.createLongArray();
		}
	}
	
	public static class VoteGift extends GiftCommand {

		private VoteType mVoteType;

		public VoteGift(Gift gift, VoteType type) {
			super(gift, null);
			mVoteType=type;
		}
		
		public VoteGift(Parcel parcel) {
			super(parcel);
			mVoteType=(VoteType) parcel.readSerializable();
		}
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest,flags);
			dest.writeSerializable(mVoteType);
		}
		
		@Override
		public int describeContents() {
			return 1003;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new VoteGift(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			CombinedApi service= MasterApiListing.getInstance().getAuthenticatedService();
			switch (mVoteType) {
			case FLAGGED:
				service.flagGift(mGift.getId());
				break;
			case TOUCHED:
				service.touchGift(mGift.getId());
				break;
			case UNFLAGGED:
				service.unflagGift(mGift.getId());
				break;
			case UNTOUCHED:
				service.untouchGift(mGift.getId());
				break;
			}
			return bundle;
		}
	}
	
	public static class MostTouchingGift extends GiftCommand {
		public MostTouchingGift() {
			super(null, null);
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			ArrayList<Gift> gifts=new ArrayList<>(MasterApiListing.getInstance().getCombinedService().mostTouchingGifts());
			bundle.putParcelableArrayList(GIFT_INFO,gifts);
			bundle.putParcelable(CommandHandler.COMMAND,this);
			return bundle;
		}
		
		public MostTouchingGift(Parcel parcel) {
			super(parcel);
			Log.v(TAG,"MostTouchingGift Parcel created");
		}
		
		
		@Override
		public int describeContents() {
			return 1004;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new MostTouchingGift(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class FindByTitle extends Command {
		private String mTitle;

		public FindByTitle(String title) {
			mTitle=title;
		}
		
		@Override
		public Bundle execute() {
			Log.v(TAG,"Executing Most Touching Gift");
			Bundle bundle=new Bundle();
			ArrayList<Gift> gifts=new ArrayList<>(MasterApiListing.getInstance()
					.getCombinedService()
					.searchGiftByTitle(mTitle));
			Log.v(TAG,Arrays.toString(gifts.toArray(new Gift[]{})));
			bundle.putParcelableArrayList(GIFT_INFO,gifts);
			bundle.putParcelable(CommandHandler.COMMAND,this);
			return bundle;
		}
		
		public FindByTitle(Parcel parcel) {
			mTitle=parcel.readString();
		}
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(mTitle);
		}
		
		@Override
		public int describeContents() {
			return 1005;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new FindByTitle(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
}
