package com.kd7uiy.potlatch.client.serverApi;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;

import com.kd7uiy.library.Command;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;

import retrofit.RetrofitError;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class UserCommandWrapper {
	private static final String TAG = "userCommand";
	
	public static final String USER = "userCommand";

	public static final String USER_INFO = "user-info";
	
	public static class UserCommand extends Command {
		protected User mUser;

		public UserCommand(User user) {
			mUser=user;
		}
		
		public UserCommand(Parcel parcel) {
			super(parcel);
			mUser=parcel.readParcelable(User.class.getClassLoader());
		}
		
		@Override
		public int describeContents() {
			return 3001;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(mUser,flags);
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new UserCommand(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};

		@Override
		public Bundle execute() {
			// Does nothing, has to be non-abstract to make this work correctly...
			return null;
		}
	}

	public static class NewUser extends UserCommand {

		public NewUser(User user) {
			super(user);
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			User user=MasterApiListing.getInstance().getCombinedService().newUser(mUser);
			bundle.putParcelable(USER,user);
			return bundle;
		}
		
		public NewUser(Parcel parcel) {
			super(parcel);
		}
		
		@Override
		public int describeContents() {
			return 3002;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new NewUser(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class UpdateUser extends UserCommand {

		public UpdateUser(User user) {
			super(user);
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			User user=MasterApiListing.getInstance().getCombinedService().updateUser(mUser);
			bundle.putParcelable(USER,user);
			return bundle;
		}
		
		public UpdateUser(Parcel parcel) {
			super(parcel);
		}
		
		@Override
		public int describeContents() {
			return 3003;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new UpdateUser(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class CurrentUser extends UserCommand {

		public CurrentUser() {
			super((User) null);
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			try {
				User user=MasterApiListing.getInstance().getAuthenticatedService().getCurrentUser();
				Log.v(TAG,"current_user="+user);
				ArrayList<GiftChain> chains=new ArrayList<>(MasterApiListing
						.getInstance()
						.getCombinedService()
						.getChainsFromUser(user.getId()));
				bundle.putParcelable(USER,user);
				bundle.putParcelableArrayList(GiftChainCommandWrapper.GIFT_CHAIN_INFO,chains);
			} catch (RetrofitError e) {
				bundle.putParcelable(USER,null);
			}
			return bundle;
		}
		
		public CurrentUser(Parcel parcel) {
			super(parcel);
		}
		
		@Override
		public int describeContents() {
			return 3004;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new CurrentUser(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class GetGifts  extends Command {
		
		private long mUserId;

		public GetGifts(long userId) {
			mUserId=userId;
		}
		
		@Override
		public int describeContents() {
			return 3005;
		}
		
		public GetGifts(Parcel parcel) {
			mUserId=parcel.readLong();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(mUserId);
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			ArrayList<Gift> gifts=new ArrayList<Gift>(MasterApiListing
					.getInstance()
					.getCombinedService()
					.getGiftsFromUser(mUserId));
			bundle.putParcelableArrayList(GiftCommandWrapper.GIFT_INFO,gifts);
			bundle.putParcelable(CommandHandler.COMMAND,this);
			return bundle;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new GetGifts(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class GetUsers extends Command {
		
		public GetUsers() {
			super();
		}
		
		@Override
		public int describeContents() {
			return 3006;
		}
		
		public GetUsers(Parcel parcel) {
			super(parcel);
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			ArrayList<User> users=new ArrayList<User>(MasterApiListing
					.getInstance()
					.getCombinedService()
					.getUsers());
			bundle.putParcelableArrayList(USER_INFO,users);
			return bundle;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new GetUsers(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
}
