package com.kd7uiy.potlatch.client.serverApi;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;

import com.kd7uiy.library.Command;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class GiftChainCommandWrapper {
	
	public static final String CHAIN = "chain";
	public static final String GIFT_CHAIN_INFO = "chain-info";
	
	public static class ChainCommand extends Command {
		protected GiftChain mGiftChain;

		public ChainCommand(GiftChain chain) {
			mGiftChain=chain;
		}
		
		public ChainCommand(Parcel parcel) {
			super(parcel);
			mGiftChain=parcel.readParcelable(GiftChain.class.getClassLoader());
		}
		
		@Override
		public int describeContents() {
			return 2001;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeParcelable(mGiftChain,flags);
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new ChainCommand(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};

		@Override
		public Bundle execute() {
			// Does nothing, has to be non-abstract to make this work correctly...
			return null;
		}
	}
	
	public static class NewChainWithGift extends ChainCommand {

		private Gift mGift;

		public NewChainWithGift(GiftChain chain, Gift gift) {
			super(chain);
			mGift=gift;
		}
		
		@Override
		public int describeContents() {
			return 2006;
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			GiftChain chain=MasterApiListing.getInstance().getCombinedService().createChain(mGiftChain);
			MasterApiListing.getInstance().getCombinedService().addToChain(chain.getId(),mGift.getId());
			bundle.putParcelable(CHAIN,chain);
			return bundle;
		}
		
		public NewChainWithGift(Parcel parcel) {
			super(parcel);
			mGift=parcel.readParcelable(Gift.class.getClassLoader());
		}
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest,flags);
			dest.writeParcelable(mGift,flags);
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new NewChainWithGift(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}

	public static class NewChain extends ChainCommand {

		public NewChain(GiftChain user) {
			super(user);
		}
		
		@Override
		public int describeContents() {
			return 2002;
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			GiftChain chain=MasterApiListing.getInstance().getCombinedService().createChain(mGiftChain);
			bundle.putParcelable(CHAIN,chain);
			return bundle;
		}
		
		public NewChain(Parcel parcel) {
			super(parcel);
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new NewChain(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class GetChains extends Command {

		public GetChains() {
		}
		
		@Override
		public int describeContents() {
			return 2004;
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			ArrayList<GiftChain> chains=new ArrayList<GiftChain>(MasterApiListing.getInstance().getCombinedService().getChains());
			bundle.putParcelableArrayList(GIFT_CHAIN_INFO,chains);
			bundle.putParcelable(CommandHandler.COMMAND,this);
			return bundle;
		}
		
		public GetChains(Parcel parcel) {
			super(parcel);
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new GetChains(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class GetGifts  extends Command {
		
		private long mChainId;

		public GetGifts(long chainId) {
			mChainId=chainId;
		}
		
		@Override
		public int describeContents() {
			return 2005;
		}
		
		public GetGifts(Parcel parcel) {
			mChainId=parcel.readLong();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(mChainId);
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			ArrayList<Gift> gifts=new ArrayList<Gift>(MasterApiListing
					.getInstance()
					.getCombinedService()
					.getGiftsFromChain(mChainId));
			bundle.putParcelable(CommandHandler.COMMAND,this);
			bundle.putParcelableArrayList(GiftCommandWrapper.GIFT_INFO,gifts);
			return bundle;
		}
		
		public static final Parcelable.Creator<Command> CREATOR
		= new Parcelable.Creator<Command>() {
			public Command createFromParcel(Parcel in) {
				return new GetGifts(in);
			}

			public Command[] newArray(int size) {
				return new Command[size];
			}
		};
	}
	
	public static class AddToChain extends Command{
		
		long mGiftId;
		long mChainId;

		public AddToChain(long chainId, long giftId) {
			mGiftId=giftId;
			mChainId=chainId;
		}
		
		@Override
		public Bundle execute() {
			Bundle bundle=new Bundle();
			MasterApiListing.getInstance()
				.getCombinedService()
				.addToChain(mChainId,mGiftId);
			return bundle;
		}
		
		public AddToChain(Parcel parcel) {
			mChainId=parcel.readLong();
			mGiftId=parcel.readLong();
		}
		
		@Override
		public int describeContents() {
			return 2003;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(mChainId);
			dest.writeLong(mGiftId);
		}
		
		public static final Parcelable.Creator<AddToChain> CREATOR
		= new Parcelable.Creator<AddToChain>() {
			public AddToChain createFromParcel(Parcel in) {
				return new AddToChain(in);
			}

			public AddToChain[] newArray(int size) {
				return new AddToChain[size];
			}
		};
	}
}
