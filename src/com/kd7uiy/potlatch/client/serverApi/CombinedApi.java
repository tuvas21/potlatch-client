package com.kd7uiy.potlatch.client.serverApi;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.List;

import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Streaming;
import retrofit.mime.TypedFile;

public interface CombinedApi {

	public static final String GIFT_SVC_PATH="/gift";
	public static final String TOKEN_PATH = "/oauth/token";
	public static final String CHAIN_SVC_PATH="/chain";
	
	//Basic Server APIs
	@POST("/oauth/token/revoke")
	public Object logout();
	
	
	//USER APIs
	@POST("/user/new")
	public User newUser(@Body User user);
	
	@POST("/user/update")
	public User updateUser(@Body User user);
	
	@POST("/user")
	public User getCurrentUser();
	
	@GET("/user/{id}/gifts")
	public List<Gift> getGiftsFromUser(@Path("id") long id);
	
	@GET("/user/{id}/chains")
	public List<GiftChain> getChainsFromUser(@Path("id") long id);
	
	@GET("/user")
	public List<User> getUsers();
	
	
	//CHAIN APIs
	@GET(CHAIN_SVC_PATH)
	public List<GiftChain> getChains();
	
	@POST(CHAIN_SVC_PATH+"/create")
	public GiftChain createChain(@Body GiftChain chain);
	
	@POST(CHAIN_SVC_PATH+"/{id}/add")
	public boolean addToChain(@Path("id") long chainId, @Body long giftId);
	
	@GET(CHAIN_SVC_PATH+"/{id}")
	public List<Gift> getGiftsFromChain(@Path("id") long id);
	
	@Streaming
	@GET(CHAIN_SVC_PATH+"/{id}/get")
	public Response downloadGiftChainImage(@Path("id") long id);
	
	
	//GIFT APIs
	@GET(GIFT_SVC_PATH)
	public List<Gift> getGifts();
	
	@POST(GIFT_SVC_PATH+"/create")
	public Gift createGift(@Body Gift gift);
	
	@GET(GIFT_SVC_PATH+"/{id}")
	public Gift getGiftInfo(@Path("id") long giftId);
	
	@Multipart
	@POST(GIFT_SVC_PATH+"/{id}/upload")
	public boolean uploadGiftImage(@Path("id") long id, @Part("photo") TypedFile photo);
	
	@Streaming
	@GET(GIFT_SVC_PATH+"/{id}/get")
	public Response downloadGiftImage(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH+"/{id}/touch")
	public boolean touchGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH+"/{id}/untouch")
	public boolean untouchGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH+"/{id}/flag")
	public boolean flagGift(@Path("id") long id);
	
	@POST(GIFT_SVC_PATH+"/{id}/unflag")
	public boolean unflagGift(@Path("id") long id);
	
	
	//TODO Properly Test
	@GET(GIFT_SVC_PATH+"/touching")
	public List<Gift> mostTouchingGifts();
	
	
	//TODO Properly Test
	@GET(GIFT_SVC_PATH+"/search/{title}")
	public List<Gift> searchGiftByTitle(@Path("title")String title);
	
	
	//TODO Properly test
	@GET(GIFT_SVC_PATH+"/{id}/touchedUsers")
	public List<User> getGiftTouchedUsers(@Path("id")long id);
	
	
}
