package com.kd7uiy.potlatch.client.fragments;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.MainActivity;
import com.kd7uiy.potlatch.client.MainActivity.TabType;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.adapters.GiftImage;
import com.kd7uiy.potlatch.client.dialog.LoginDialogFragment;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.controller.UserManager;
import com.kd7uiy.potlatch.shared.Gift;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CreateGiftFragment extends Fragment implements OnClickListener{
	
	@SuppressWarnings("unused")
	private static final String TAG="CreateGift";

	private String mImagePath=null;
	private Uri mImageUri=null;

	private ImageView mImageView;
	private TextView mDescription;
	private TextView mTitle;

	private Button mButton;
	
	
	public void setImage(Uri uri) {
		if (isAdded()) {
			mImagePath=getPath(uri);
			setImageView();
		} else {
			mImageUri=uri;
		}
	}

	
	private String getPath(Uri uri) 
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

	public void setImageView() {
		new SetImageTask().execute(mImagePath);
	}
	
	private class SetImageTask extends AsyncTask<String,Void,Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {
			return BitmapFactory.decodeFile(params[0]);
		}
		
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			mImageView.setImageBitmap(bitmap);
			mImageView.requestLayout();
		}
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setRetainInstance(true);
		View rootView = inflater.inflate(R.layout.gift_create, container,
				false);
		
		mImageView=(ImageView) rootView.findViewById(R.id.image);
		mTitle= (TextView) rootView.findViewById(R.id.title);
		mDescription= (TextView) rootView.findViewById(R.id.description);
		mButton= (Button) rootView.findViewById(R.id.submitButton);
		mButton.setOnClickListener(this);
		mImageView.setOnClickListener(this);

		if (mImageUri!=null) {
			mImagePath=getPath(mImageUri);
			setImageView();
		}
		
		return rootView;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.submitButton:
			if (UserManager.getInstance().getUser()==null) {
				new LoginDialogFragment().show(getFragmentManager(),"login");
			} else {
				Gift gift=new Gift();
				gift.setTitle(mTitle.getText().toString());
				gift.setDescription(mDescription.getText().toString());
				
				Intent intent=ThreadPoolCommandService.makeIntent(getActivity(), new CommandHandler(getActivity()),
						CommandHandler.UPLOAD_GIFT,
						new GiftCommandWrapper.CreateGift(gift,mImagePath,getActivity().getCacheDir()), null);
				getActivity().startService(intent);
				
				if (getActivity() instanceof MainActivity) {
					((MainActivity) getActivity()).switchTab(TabType.GIFT);
					Bitmap bitmap=BitmapFactory.decodeFile(mImagePath);
					((GridViewGiftViewerFragment) ((MainActivity) getActivity()).getFragment(TabType.GIFT)).add(new GiftImage(gift,bitmap));
				}
			}
			break;
		case R.id.image:
			// Create an image file name
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
			String imageFileName = "JPEG_" + timeStamp + "_";
			
			Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File storageDir = Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_PICTURES);
			storageDir.mkdirs();
			File imageFile = new File(storageDir,
			        imageFileName+  /* prefix */
			        ".jpg"         /* suffix */
			    );
			mImagePath=imageFile.getAbsolutePath();
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
			getActivity().startActivityForResult(intent,MainActivity.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
		}
	}

}
