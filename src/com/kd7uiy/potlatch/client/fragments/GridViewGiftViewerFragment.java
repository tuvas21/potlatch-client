package com.kd7uiy.potlatch.client.fragments;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.kd7uiy.library.Command;
import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.adapters.GiftImage;
import com.kd7uiy.potlatch.client.adapters.GiftViewAdapter;
import com.kd7uiy.potlatch.client.adapters.GiftViewAdapter.AdapterType;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.shared.Gift;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class GridViewGiftViewerFragment extends Fragment implements OnRefreshListener, OnSharedPreferenceChangeListener{
	
	@SuppressWarnings("unused")
	private static final String TAG="MultipleGiftViewer";
	
	private GiftViewAdapter mAdapter;
	private SwipeRefreshLayout mSwipeRefresh;
	
	private Command mRefreshCommand=new GiftCommandWrapper.MostTouchingGift();

	private BroadcastReceiver mRefreshCommandReceiver= new RefreshCommandReceiver();
	private BroadcastReceiver mGiftInfoUpdateReceiver= new UpdateGiftInfoReceiver();

	private int mUpdatePeriod;

	private ScheduledThreadPoolExecutor mUpdateManager=new ScheduledThreadPoolExecutor(1);

	private ScheduledFuture<?> mFuture=null;

	private CommandHandler mHandler;

	public void setGifts(List<Gift> gifts, Command refreshCommand) {
		mAdapter.clear();
		mAdapter.add(gifts);
		
		mRefreshCommand=refreshCommand;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		setRetainInstance(true);
		mHandler=new CommandHandler(activity);
		
		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(activity);
		lbm.registerReceiver(mRefreshCommandReceiver,new IntentFilter(CommandHandler.GIFT_INFO_DOWNLOADED));
		lbm.registerReceiver(mGiftInfoUpdateReceiver,new IntentFilter(CommandHandler.GIFT_INFO_UPDATE));
		
		SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(activity);
		mUpdatePeriod=Integer.parseInt(prefs.getString("sync_frequency","60"));
		prefs.registerOnSharedPreferenceChangeListener(this);
		
		updateTimer();
	}

	private void updateTimer() {
		if (mFuture!=null) {
			mFuture.cancel(false);
		}
		
		mFuture=mUpdateManager.scheduleAtFixedRate(new Runnable(){
			@Override
			public void run() {
				update();
			}},mUpdatePeriod,mUpdatePeriod,TimeUnit.SECONDS);
	}
	
	@Override
	public void onDetach() {
		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(getActivity());
		lbm.unregisterReceiver(mRefreshCommandReceiver);
		super.onDetach();
	}
	
	private class RefreshCommandReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			mRefreshCommand=intent.getExtras().getParcelable(CommandHandler.COMMAND);
		}
	}
	
	private class UpdateGiftInfoReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			ArrayList<Gift> gifts=intent.getExtras().getParcelableArrayList(GiftCommandWrapper.GIFT_INFO);
			for (Gift gift: gifts) {
				mAdapter.update(gift);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.gift_gridview, container,
				false);
		mSwipeRefresh=(SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);
		mAdapter= new GiftViewAdapter(getActivity(),AdapterType.GIFT_SMALL);
		GridView grid=(GridView) rootView.findViewById(R.id.gridview);
		mAdapter.setParentView(grid);
		if (savedInstanceState != null) {
			 mAdapter.onRestoreInstanceState(savedInstanceState);
		}
		
		mSwipeRefresh.setOnRefreshListener(this);
		grid.setAdapter(mAdapter);
		grid.setOnItemClickListener(mAdapter);
		
		return rootView;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		 super.onSaveInstanceState(outState);
		 mAdapter.onSaveInstanceState(outState);
	}

	@Override
	public void onRefresh() {
		//This will probably need to be changed eventually to just refresh the current view.
		update();
		mSwipeRefresh.setRefreshing(false);
		
	}

	private void update() {
		Intent intent=ThreadPoolCommandService.makeIntent(getActivity(), mHandler,
				CommandHandler.GIFT_INFO_UPDATED,mRefreshCommand, null);
		getActivity().startService(intent);
	}

	public void add(Gift gift) {
		mAdapter.add(gift);
	}

	public void add(GiftImage giftImage) {
		mAdapter.add(giftImage);
		
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs,
			String key) {
		if (key.equals("sync_frequency")) {
			mUpdatePeriod=Integer.parseInt(prefs.getString("sync_frequency","60"));
			updateTimer();
		}
	}

}
