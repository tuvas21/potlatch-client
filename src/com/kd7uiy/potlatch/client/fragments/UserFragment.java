package com.kd7uiy.potlatch.client.fragments;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.MainActivity;
import com.kd7uiy.potlatch.client.MainActivity.TabType;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.adapters.UserViewAdapter;
import com.kd7uiy.potlatch.client.adapters.UserViewAdapter.AdapterType;
import com.kd7uiy.potlatch.client.serverApi.UserCommandWrapper;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class UserFragment extends ListFragment implements SwipeRefreshLayout.OnRefreshListener{
	@SuppressWarnings("unused")
	private static final String TAG="MultipleGiftViewer";
	
	private UserViewAdapter mAdapter;
	private SwipeRefreshLayout mSwipeRefresh;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setRetainInstance(true);
		View rootView = inflater.inflate(R.layout.swipe_refresh_listview, container,
				false);
		mSwipeRefresh=(SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);
		mAdapter= new UserViewAdapter(getActivity(), AdapterType.USER_SMALL);
		 if (savedInstanceState != null) {
			 mAdapter.onRestoreInstanceState(savedInstanceState);
		 }
		
		
		mSwipeRefresh.setOnRefreshListener(this);
		setListAdapter(mAdapter);
	
		return rootView;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		 super.onSaveInstanceState(outState);
		 mAdapter.onSaveInstanceState(outState);
	}

	@Override
	public void onRefresh() {
		//This will probably need to be changed eventually to just refresh the current view.
		Intent intent=ThreadPoolCommandService.makeIntent(getActivity(), new CommandHandler(getActivity()),
				CommandHandler.USER_INFO,
				new UserCommandWrapper.GetUsers(), null);
		getActivity().startService(intent);
		mSwipeRefresh.setRefreshing(false);
		
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent=ThreadPoolCommandService.makeIntent(getActivity(), new CommandHandler(getActivity()),
				CommandHandler.DOWNLOAD_GIFT_INFO,
				new UserCommandWrapper.GetGifts(id), null);
		getActivity().startService(intent);
		((MainActivity) getActivity()).switchTab(TabType.GIFT);
	}
}
