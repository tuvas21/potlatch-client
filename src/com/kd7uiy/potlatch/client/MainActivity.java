package com.kd7uiy.potlatch.client;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.FontsOverride;
import com.kd7uiy.library.TabsAdapter;
import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.dialog.LoginDialogFragment;
import com.kd7uiy.potlatch.client.dialog.NewAccountDialogFragment;
import com.kd7uiy.potlatch.client.fragments.CreateGiftFragment;
import com.kd7uiy.potlatch.client.fragments.GiftChainFragment;
import com.kd7uiy.potlatch.client.fragments.GridViewGiftViewerFragment;
import com.kd7uiy.potlatch.client.fragments.UserFragment;
import com.kd7uiy.potlatch.client.serverApi.GiftChainCommandWrapper.GetChains;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.UserCommandWrapper;
import com.kd7uiy.potlatch.controller.UserManager;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;

public class MainActivity extends Activity {
	private static final String TAG = "mainActivity";
	
	public enum TabType{GIFT,USER,CHAIN,CREATE};

	public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE=101;
	public static final String BACK_BUTTON_PRESSED = "back-button-pressed";

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
//	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	private ViewPager mPager;

	private TabsAdapter mPagerAdapter;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

//		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
//				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		
		final ActionBar actionBar = getActionBar();
		
		

		mPager = (ViewPager) findViewById(R.id.mainDisplay);
		if (mPager != null) {
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			actionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
			mPagerAdapter = new TabsAdapter(this, mPager);
			mPagerAdapter.addTab(
					actionBar.newTab().setText(R.string.tab_giftChain),
					GiftChainFragment.class, null);
			mPagerAdapter.addTab(
					actionBar.newTab().setText(R.string.tab_user),
					UserFragment.class, null);
			mPagerAdapter.addTab(
					actionBar.newTab().setText(R.string.tab_giftViewer),
					GridViewGiftViewerFragment.class, null);
			mPagerAdapter.addTab(
					actionBar.newTab().setText(R.string.tab_giftCreator),
					CreateGiftFragment.class, null);
			

			mPager.setAdapter(mPagerAdapter);

			if (savedInstanceState != null) {
				actionBar.setSelectedNavigationItem(savedInstanceState.getInt(
						"tab", 0));
			}
		}
		
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		
		
		registerBroadcastListeners();
	    handleIntent(getIntent());
	    
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			FontsOverride.setDefaultFont(this, "DEFAULT", "Roboto-Light.ttf");
			FontsOverride.setDefaultFont(this, "DEFAULT_BOLD",
					"Roboto-Regular.ttf");
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
	    setIntent(intent);
	    handleIntent(intent);
	}

	private void handleIntent(Intent startIntent) {
		String action= startIntent.getAction();
		String type = startIntent.getType();
		
		if (Intent.ACTION_SEND.equals(action) && type!=null) {
			//This processes incoming images.
			//For now, assume it is an image.
			switchTab(TabType.CREATE);
			CreateGiftFragment frag= (CreateGiftFragment) getFragment(TabType.CREATE);
			Uri uri=(Uri) startIntent.getParcelableExtra(Intent.EXTRA_STREAM);
			Log.v(TAG,"data="+uri);
			frag.setImage(uri);
		} else if (Intent.ACTION_SEARCH.equals(startIntent.getAction())) {
		      String query = startIntent.getStringExtra(SearchManager.QUERY);
		      switchTab(TabType.GIFT);
		      Intent intent=ThreadPoolCommandService.makeIntent(this, new CommandHandler(this),
						CommandHandler.DOWNLOAD_GIFT_INFO,
						new GiftCommandWrapper.FindByTitle(query), null);
				startService(intent);
		} else {
			Intent intent=ThreadPoolCommandService.makeIntent(this, new CommandHandler(this),
					CommandHandler.CHAIN_INFO,
					new GetChains(), null);
			startService(intent);
			
			intent=ThreadPoolCommandService.makeIntent(this, new CommandHandler(this),
					CommandHandler.DOWNLOAD_GIFT_INFO,
					new GiftCommandWrapper.MostTouchingGift(), null);
			startService(intent);
			
			intent=ThreadPoolCommandService.makeIntent(this, new CommandHandler(this),
					CommandHandler.USER_INFO,
					new UserCommandWrapper.GetUsers(), null);
			startService(intent);
		}
	}

	private void registerBroadcastListeners() {
		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(this);
		UserManager.getInstance().registerBroadcastListener(lbm);
	}
	
	public int getTab(TabType tabType) {
		switch (tabType) {
		case CHAIN:
			return 0;
		case GIFT:
			return 2;
		case USER:
			return 1;
		case CREATE:
			return 3;
		default:
			return -1;
		}
	}
	
	public Fragment getFragment(TabType tabType) {
		return mPagerAdapter.getItem(getTab(tabType));
	}
	
	@SuppressWarnings("deprecation")
	public void switchTab(TabType tabType) {
		getActionBar().setSelectedNavigationItem(getTab(tabType));
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onSaveInstanceState(Bundle out) {
		if (mPager != null) {
			out.putInt("tab", getActionBar()
					.getSelectedNavigationIndex());
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

//		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			
		    // Get the SearchView and set the searchable configuration
		    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		    SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
		    // Assumes current activity is the searchable activity
		    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		    searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
			return true;
//		}
//		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id) {
		case R.id.action_settings:
			intent=new Intent(this,SettingsActivity.class);
			startActivity(intent);
			return true;
		case R.id.action_new_account:
			new NewAccountDialogFragment().show(getFragmentManager(),"newAccount");
			return true;
		case R.id.action_login:
			new LoginDialogFragment().show(getFragmentManager(),"login");
			return true;
		} 
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(this);
		Intent intent=new Intent(BACK_BUTTON_PRESSED);
		lbm.sendBroadcast(intent);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
	    	CreateGiftFragment frag=(CreateGiftFragment) mPagerAdapter.getItem(getTab(TabType.CREATE));
	    	frag.setImageView();
	    }
	}
}
