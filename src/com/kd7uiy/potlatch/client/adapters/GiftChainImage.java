package com.kd7uiy.potlatch.client.adapters;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.potlatch.shared.GiftChain;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class GiftChainImage implements Parcelable{
	private GiftChain mGiftChain;
	private Bitmap mBitmap;
	public GiftChainImage(GiftChain giftChain, Bitmap bitmap) {
		mGiftChain=giftChain;
		mBitmap=bitmap;
	}
	public Bitmap getBitmap() {
		return mBitmap;
	}
	public void setBitmap(Bitmap bitmap) {
		mBitmap = bitmap;
	}
	public GiftChain getGiftChain() {
		return mGiftChain;
	}
	public void setGiftChain(GiftChain giftChain) {
		mGiftChain = giftChain;
	}
	
	
	public GiftChainImage(Parcel in) {
		mGiftChain=in.readParcelable(GiftChain.class.getClassLoader());
		mBitmap=in.readParcelable(Bitmap.class.getClassLoader());
	}
	@Override
	public int describeContents() {
		return 101;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(mGiftChain,flags);
		dest.writeParcelable(mBitmap,flags);
	}
	
	public static final Parcelable.Creator<GiftChainImage> CREATOR
	= new Parcelable.Creator<GiftChainImage>() {
		public GiftChainImage createFromParcel(Parcel in) {
			return new GiftChainImage(in);
		}

		public GiftChainImage[] newArray(int size) {
			return new GiftChainImage[size];
		}
	};
}