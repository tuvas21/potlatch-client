package com.kd7uiy.potlatch.client.adapters;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.library.ZoomViewAdapter;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper.DownloadGift;
import com.kd7uiy.potlatch.client.views.LargeGiftViewHelper;
import com.kd7uiy.potlatch.client.views.SmallGiftViewHelper;
import com.kd7uiy.potlatch.shared.Gift;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

// Should be CursorAdapter, but for quick demo purposes, this will work...
public class GiftViewAdapter extends ZoomViewAdapter<GiftImage> implements OnSharedPreferenceChangeListener{
	
	public enum AdapterType {GIFT_SMALL,GIFT_LARGE, GIFT_LIST_VIEW};
	protected EnumMap<AdapterType,Integer> mLayoutIdMap = new EnumMap<AdapterType,Integer>(AdapterType.class);
	protected AdapterType mType;
	
	protected boolean mFilter;
	
	public GiftViewAdapter(Context context, AdapterType size){
		super(context);
		setType(size);

		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(context);
		lbm.registerReceiver(new NewGiftReceiver(),new IntentFilter(CommandHandler.GIFT_INFO_DOWNLOADED));
		lbm.registerReceiver(new NewGiftImageReceiver(),new IntentFilter(CommandHandler.GIFT_IMAGE_DOWNLOADED));
		
		SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(context);
		mFilter=prefs.getBoolean("filter_offensive",false);
		prefs.registerOnSharedPreferenceChangeListener(this);
	}
	
	private class NewGiftReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			ArrayList<Gift> gifts=intent.getExtras().getParcelableArrayList(GiftCommandWrapper.GIFT_INFO);
			
			clear();
			add(gifts);
		}
	}
	
	private class NewGiftImageReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				Bitmap bitmap=intent.getExtras().getParcelable(GiftCommandWrapper.BITMAP);
				long giftId=intent.getExtras().getLong(GiftCommandWrapper.GIFT_ID);
				populateBitmap(giftId,bitmap);
			} catch (IndexOutOfBoundsException ex) {
			}
		}
	}
	
	@Override
	public long getItemId(int position) {
		return ((GiftImage) getItem(position)).getGift().getId();
	}

	public void update(Gift gift) {
		int pos=getPositionByItemId(gift.getId());
		if (pos!=-1) {
			GiftImage img=(GiftImage) getItem(pos);
			img.setGift(gift);
			notifyDataSetChanged();
		} else {
			add(gift);
		}
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		boolean inflateView=(view==null || !mLayoutIdMap.containsKey(mType) || mLayoutIdMap.get(mType)!=view.getId());
		switch (mType) {
		case GIFT_SMALL:
			if (inflateView) {
				view=LayoutInflater.from(mContext).inflate(R.layout.small_gift_layout,parent,false);
			}
			SmallGiftViewHelper.populateGift(view,((GiftImage) getItem(position)).getGift());
			SmallGiftViewHelper.setImage(view,((GiftImage) getItem(position)).getBitmap());
			break;
		case GIFT_LARGE:
			if (inflateView) {
				view=LayoutInflater.from(mContext).inflate(R.layout.large_gift_layout,parent,false);
			}
			LargeGiftViewHelper.populateGift(view,((GiftImage) getItem(position)).getGift());
			LargeGiftViewHelper.setImage(view,((GiftImage) getItem(position)).getBitmap());
			break;
		case GIFT_LIST_VIEW:
			//TODO Put in a list view (Used for searching)
			break;
		}
		if (!mLayoutIdMap.containsKey(mType)) {
			mLayoutIdMap.put(mType,view.getId());
		}
		return view;
	}

	public void add(GiftImage gift) {
		
		if (!(mFilter && gift.getGift().getFlagCount()>0)) {
			super.add(gift);
			notifyDataSetChanged();
		}
	}

	public void add(List<Gift> gifts) {
		for (Gift gift: gifts){
			add(gift);
		}
	}

	public void add(Gift gift) {
		add(new GiftImage(gift,null));
		Intent intent=ThreadPoolCommandService.makeIntent(mContext, new CommandHandler(mContext), CommandHandler.DOWNLOAD_GIFT_IMAGE, new DownloadGift(gift), null);
		mContext.startService(intent);
	}
	
	private void add(ArrayList<GiftImage> images) {
		for (GiftImage image: images) {
			add(image);
		}
	}
	
	public void populateBitmap(long itemId, Bitmap bitmap) {
		int pos=getPositionByItemId(itemId);
		GiftImage img=(GiftImage) getItem(pos);
		img.setBitmap(bitmap);
		notifyDataSetChanged();
		
	}

	private int getPositionByItemId(long itemId) {
		int pos=0;
		for (GiftImage img: getAllItems()) {
			if (img.getGift().getId()==itemId) {
				return pos;
			}
			pos++;
		}
		return -1;
	}

	public void clear() {
		super.clear();
		notifyDataSetChanged();
	}

	public AdapterType getType() {
		return mType;
	}

	public void setType(AdapterType type) {
		this.mType = type;
	}

	@Override
	protected CardView populateImage(View parent, int position) {
		GiftImage giftImage=(GiftImage) getItem(position);
	    // Load the high-resolution "zoomed-in" image.
	    final CardView expandedImageView = (CardView) parent.findViewById(R.id.expanded_view);
		LargeGiftViewHelper.populateGift(expandedImageView,giftImage.getGift());
		LargeGiftViewHelper.setImage(expandedImageView,giftImage.getBitmap());
		return expandedImageView;
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs,
			String key) {
		if (key.equals("filter_offensive")) {
			mFilter=prefs.getBoolean("filter_offensive",false);
			
			//This will update the images displayed if the filter flag is set to true
			ArrayList<GiftImage> temp=getAllItems();
			clear();
			add(temp);
		}
		
	}
}
