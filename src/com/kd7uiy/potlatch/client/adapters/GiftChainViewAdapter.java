package com.kd7uiy.potlatch.client.adapters;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.kd7uiy.library.StateSavingArrayAdapter;
import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.dialog.LoginDialogFragment;
import com.kd7uiy.potlatch.client.dialog.NewGiftChainDialogFragment;
import com.kd7uiy.potlatch.client.serverApi.GiftChainCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper.DownloadGift;
import com.kd7uiy.potlatch.client.views.GiftChainListViewHelper;
import com.kd7uiy.potlatch.controller.UserManager;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftChain;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

// Should be CursorAdapter, but for quick demo purposes, this will work...
public class GiftChainViewAdapter extends
		StateSavingArrayAdapter<GiftChainImage> {

	public enum AdapterType {
		GIFT_CHAIN_SMALL, GIFT_CHAIN_LIST_VIEW
	}

	public static final long NEW_CHAIN = 0;
	private static final String TAG = "GiftChainAdapter";
	protected EnumMap<AdapterType, Integer> mLayoutIdMap = new EnumMap<AdapterType, Integer>(
			AdapterType.class);
	private Context mContext;
	private AdapterType mType;

	public GiftChainViewAdapter(Context context, AdapterType type) {
		super();
		init(context, type);

		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(context);
		lbm.registerReceiver(new NewGiftChainReceiver(), new IntentFilter(
				CommandHandler.GIFT_CHAIN_INFO_DOWNLOADED));
		lbm.registerReceiver(new NewGiftImageReceiver(), new IntentFilter(
				CommandHandler.GIFT_IMAGE_DOWNLOADED));
	}

	private void init(Context context, AdapterType type) {
		mContext = context;
		setType(type);

		add(getDefaultImage());
	}

	private GiftChainImage getDefaultImage() {
		GiftChain chain = new GiftChain();
		chain.setName(mContext.getString(R.string.create_new_gift_chain));
		chain.setId(NEW_CHAIN);
		Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),
				R.drawable.ic_add_card);
		return new GiftChainImage(chain, bitmap);
	}

	public GiftChainViewAdapter(Context context, AdapterType type,
			ArrayList<GiftChain> giftChains) {
		super();
		init(context, type);
		add(giftChains);
	}

	private class NewGiftChainReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			ArrayList<GiftChain> chains = intent.getExtras()
					.getParcelableArrayList(
							GiftChainCommandWrapper.GIFT_CHAIN_INFO);
			clear();
			add(chains);
		}
	}

	private class NewGiftImageReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bitmap bitmap = intent.getExtras().getParcelable(
					GiftCommandWrapper.BITMAP);
			long giftId = intent.getExtras()
					.getLong(GiftCommandWrapper.GIFT_ID);
			populateBitmap(giftId, bitmap);
		}

	}

	public class GiftImage {
		private Gift mGift;
		private Bitmap mBitmap;

		public GiftImage(Gift gift, Bitmap bitmap) {
			mGift = gift;
			mBitmap = bitmap;
		}

		public Bitmap getBitmap() {
			return mBitmap;
		}

		public void setBitmap(Bitmap bitmap) {
			mBitmap = bitmap;
		}

		public Gift getGift() {
			return mGift;
		}

		public void setGift(Gift gift) {
			mGift = gift;
		}
	}

	@Override
	public long getItemId(int pos) {
		Log.v(TAG,"Id="+((GiftChainImage) getItem(pos)).getGiftChain().getId());
		return ((GiftChainImage) getItem(pos)).getGiftChain().getId();
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		boolean inflateView = (view == null || !mLayoutIdMap.containsKey(mType) || mLayoutIdMap
				.get(mType) != view.getId());
		switch (mType) {
		case GIFT_CHAIN_SMALL:
		case GIFT_CHAIN_LIST_VIEW:
			if (inflateView) {
				view = LayoutInflater.from(mContext).inflate(
						R.layout.gift_chain_list_view_item, parent, false);
			}
			GiftChainListViewHelper.populateInformation(view,
					((GiftChainImage) getItem(position)).getGiftChain());
			GiftChainListViewHelper.setImage(view,
					((GiftChainImage) getItem(position)).getBitmap());
			break;
		}
		if (!mLayoutIdMap.containsKey(mType)) {
			mLayoutIdMap.put(mType, view.getId());
		}
		return view;
	}

	public void add(GiftChainImage giftChain) {
		if (!(UserManager.getInstance().blockFlagged())) {
			// Adds to the beginning of the chain
			super.add(0, giftChain);
			notifyDataSetChanged();
		}
	}

	public void add(List<GiftChain> chains) {
		for (GiftChain chain : chains) {
			add(chain);
		}
	}

	private void add(GiftChain chain) {
		add(new GiftChainImage(chain, null));
		//TODO The above code should be in the if statement, and use a default image in the else clause.
		if (chain.getDefaultImage()!=0) {
			Intent intent = ThreadPoolCommandService.makeIntent(mContext,
					new CommandHandler(mContext),
					CommandHandler.DOWNLOAD_GIFT_IMAGE,
					new DownloadGift(chain.getDefaultImage()), null);
			mContext.startService(intent);
		}
	}

	public void populateBitmap(long itemId, Bitmap bitmap) {
		int pos = getPositionByGiftId(itemId);
		try {
			GiftChainImage img = (GiftChainImage) getItem(pos);
			img.setBitmap(bitmap);
			super.remove(pos);
			add(pos, img);
			notifyDataSetChanged();
		} catch (ArrayIndexOutOfBoundsException ex) {

		}

	}

	private int getPositionByGiftId(long giftId) {
		int pos = 0;
		for (GiftChainImage img : getAllItems()) {
			if (img.getGiftChain().getDefaultImage() == giftId) {
				return pos;
			}
			pos++;
		}
		return -1;
	}

	public void clear() {
		super.clear();
		add(getDefaultImage());
		notifyDataSetChanged();
	}

	public AdapterType getType() {
		return mType;
	}

	public void setType(AdapterType type) {
		mType = type;
	}

	public static void newGiftChain(Gift gift, Activity activity) {
		DialogFragment frag = NewGiftChainDialogFragment.getInstance(gift);
		if (frag == null) {
			new LoginDialogFragment().show((activity).getFragmentManager(),"login");
		} else {
			frag.show((activity).getFragmentManager(), "newChain");
		}
	}

}
