package com.kd7uiy.potlatch.client.adapters;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.kd7uiy.library.StateSavingArrayAdapter;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.serverApi.UserCommandWrapper;
import com.kd7uiy.potlatch.client.views.UserListViewHelper;
import com.kd7uiy.potlatch.controller.UserManager;
import com.kd7uiy.potlatch.shared.User;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

// Should be CursorAdapter, but for quick demo purposes, this will work...
public class UserViewAdapter extends StateSavingArrayAdapter<User> {
	
	public enum AdapterType {USER_SMALL};
	protected EnumMap<AdapterType,Integer> mLayoutIdMap = new EnumMap<AdapterType,Integer>(AdapterType.class);
	private Context mContext;
	private AdapterType mType;

	
	public UserViewAdapter(Context context, AdapterType type){
		super();
		mContext=context;
		setType(type);
		
		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(context);
		lbm.registerReceiver(new NewUserReceiver(),new IntentFilter(CommandHandler.USER_INFO_DOWNLOADED));
	}
	
	private class NewUserReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			ArrayList<User> users=intent.getExtras()
					.getParcelableArrayList(UserCommandWrapper.USER_INFO);
			
			clear();
			add(users);
		}
	}

	@Override
	public long getItemId(int position) {
		return ((User) getItem(position)).getId();
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		boolean inflateView=(view==null || !mLayoutIdMap.containsKey(mType) || mLayoutIdMap.get(mType)!=view.getId());
		switch (mType) {
		case USER_SMALL:
			if (inflateView) {
				view=LayoutInflater.from(mContext).inflate(R.layout.user_list_view_item,parent,false);
			}
			UserListViewHelper.populateInformation(view,((User) getItem(position)));
//			GiftChainListViewHelper.setImage(view,((GiftChainImage) getItem(position)).getBitmap());
			break;
		}
		if (!mLayoutIdMap.containsKey(mType)) {
			mLayoutIdMap.put(mType,view.getId());
		}
		return view;
	}

	public void add(User user) {
		if (!(UserManager.getInstance().blockFlagged())) {
			super.add(user);
			notifyDataSetChanged();
		}
	}

	public void add(List<User> users) {
		for (User user: users){
			add(user);
		}
	}

//	private void add(User user) {
//		add(user);
//		Intent intent=ThreadPoolCommandService.makeIntent(mContext, new CommandHandler(mContext),
//				CommandHandler.DOWNLOAD_GIFT_IMAGE, new DownloadGift(chain.getDefaultImage()), null);
//		mContext.startService(intent);
//	}
	
//	public void populateBitmap(long itemId, Bitmap bitmap) {
//		int pos=getPositionByItemId(itemId);
//		try {
//			GiftChainImage img=(GiftChainImage) getItem(pos);
//			img.setBitmap(bitmap);
//			mUsers.remove(pos);
//			mUsers.add(pos,img);
//			notifyDataSetChanged();
//		} catch (ArrayIndexOutOfBoundsException ex) {
//			
//		}
//		
//	}

//	private int getPositionByItemId(long itemId) {
//		int pos=0;
//		for (User user: mUsers) {
//			if (user.getId()==itemId) {
//				return pos;
//			}
//			pos++;
//		}
//		return -1;
//	}

	public void clear() {
		super.clear();
		notifyDataSetChanged();
		
	}

	public AdapterType getType() {
		return mType;
	}

	public void setType(AdapterType type) {
		mType = type;
	}


}
