package com.kd7uiy.potlatch.client;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.kd7uiy.library.CommandUtils;
import com.kd7uiy.potlatch.client.dialog.GiftChainChooseFragment;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.shared.Gift;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

/**
 * This is the handler used for handling messages sent by a Messenger. It
 * receives a message containing a pathname to an image and displays that image
 * in the ImageView.
 * 
 * The handler plays several roles in the Active Object pattern, including
 * Proxy, Future, and Servant.
 */
public class CommandHandler extends Handler {
	@SuppressWarnings("unused")
	private static final String TAG = "CommandHandler";
	
	public static final String COMMAND = "command";
	
	public static final int LOGGED_IN_USER=1;
	public static final int USER_INFO=2;
//	public static final int IMAGE_DOWNLOADED_COMPLETE = 2;
	public static final int DOWNLOAD_GIFT_IMAGE = 3;
	public static final int DOWNLOAD_GIFT_INFO = 4;
	public static final int UPLOAD_GIFT = 5;
	public static final int CHAIN_INFO = 6;
	public static final int GIFT_VOTE = 7;
	public static final int CHAIN_INFO_UPDATE = 8;
	public static final int GIFT_INFO_UPDATED = 9;
	public static final int NEW_GIFT_CHAIN = 10;
	public static final int NEW_USER_CREATED = 11;
	public static final String GIFT_INFO_DOWNLOADED = "gift-info-downloaded";
	public static final String USER_INFO_UPDATED="user-info-updated";
	public static final String GIFT_IMAGE_DOWNLOADED = "gift-image-downloaded";
	public static final String GIFT_CHAIN_INFO_DOWNLOADED = "gift-chain-info-downloaded";
	public static final String USER_INFO_DOWNLOADED = "user-info-downloaded";
	public static final String GIFT_INFO_UPDATE = "gift-info-update";

	// A weak reference to the enclosing class

	private WeakReference<Context> mContext;

	/**
	 * A class constant that determines the maximum number of threads used to
	 * service download requests.
	 */
	static final int MAX_THREADS = 4;


	/**
	 * The ExecutorService that references a ThreadPool.
	 */
	static protected ExecutorService mExecutor;

	/**
	 * A constructor that gets a weak reference to the enclosing class. We do
	 * this to avoid memory leaks during Java Garbage Collection.
	 * 
	 * @see https
	 *      ://groups.google.com/forum/#!msg/android-developers/1aPZXZG6kWk/
	 *      lIYDavGYn5UJ
	 */
	public CommandHandler(Context context) {
		super();
		init(context);
	}

	public CommandHandler(Context context, Looper looper) {
		super(looper);
		init(context);
	}

	private void init(Context context) {
		mContext = new WeakReference<Context>(context);
		mExecutor = Executors.newFixedThreadPool(MAX_THREADS);
	}

	// Handle any messages that get sent to this Handler
	@Override
	public void handleMessage(Message msg) {
		// Get an actual reference to the DownloadActivity
		// from the WeakReference.
		Context context = mContext.get();
		if (context == null) {
			return;
		}
		
		LocalBroadcastManager lbm=LocalBroadcastManager.getInstance(context);
		
		Bundle bundle=msg.getData();
//		Bundle extras=bundle.getBundle(CommandUtils.EXTRAS);
		Intent intent=null;

		switch (msg.what) {
		case LOGGED_IN_USER:
			intent= new Intent(USER_INFO_UPDATED);
			break;
		case USER_INFO:
			intent = new Intent(USER_INFO_DOWNLOADED);
			break;
		case DOWNLOAD_GIFT_IMAGE:
			intent= new Intent(GIFT_IMAGE_DOWNLOADED);
			break;
		case DOWNLOAD_GIFT_INFO:
			intent= new Intent(GIFT_INFO_DOWNLOADED);
			break;
		case UPLOAD_GIFT:
			if (mContext!=null && mContext.get() instanceof Activity) {
				Gift gift=bundle.getParcelable(GiftCommandWrapper.GIFT);
				GiftChainChooseFragment.getInstance(gift).show(((Activity) mContext.get()).getFragmentManager(),"chain-chooser");
			}
			break;
		case CHAIN_INFO:
			intent= new Intent(GIFT_CHAIN_INFO_DOWNLOADED);
			break;
		case GIFT_VOTE:	//TODO implement something here
		case CHAIN_INFO_UPDATE:	//TODO implement something here
			break;
		case GIFT_INFO_UPDATED:
			intent = new Intent(GIFT_INFO_UPDATE);
			break;
		case NEW_USER_CREATED:
			
		case CommandUtils.DEFAULT_MSG_ID:
			break;
		}
		if (intent!=null) {
			if (bundle!=null ) {
				bundle.setClassLoader(getClass().getClassLoader());
				intent.putExtras(bundle);
			}
			lbm.sendBroadcast(intent);
		} else {
//			Log.w(TAG,"No return intent found for "+msg);
		}
	}


}
