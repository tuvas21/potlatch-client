package com.kd7uiy.potlatch.client.views;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.library.ThreadPoolCommandService;
import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.dialog.LoginDialogFragment;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.GiftCommandWrapper.VoteType;
import com.kd7uiy.potlatch.controller.UserManager;
import com.kd7uiy.potlatch.shared.Gift;
import com.kd7uiy.potlatch.shared.GiftVote;
import com.kd7uiy.potlatch.shared.User;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class LargeGiftViewHelper {
	
	public static final String TAG = "LargeGift";

	static public boolean populateGift(View view, Gift gift) {
		TextView title=(TextView) view.findViewById(R.id.title);
		TextView description=(TextView) view.findViewById(R.id.description);
		TextView touchedCount=(TextView) view.findViewById(R.id.touchedCount);
		TextView flaggedCount=(TextView) view.findViewById(R.id.flagCount);
		ImageView touchedCountImage=(ImageView) view.findViewById(R.id.touchedIcon);
		ImageView flaggedCountImage=(ImageView) view.findViewById(R.id.flagIcon);
		ToggleButton touchToggleButton=(ToggleButton) view.findViewById(R.id.touchedButton);
		ToggleButton flaggedToggleButton=(ToggleButton) view.findViewById(R.id.flagButton);
		
		OnClickListener listener=new VoteOnClickListener(gift);
		
		touchToggleButton.setOnClickListener(listener);
		flaggedToggleButton.setOnClickListener(listener);
		
		if (gift.getTitle()!=null) {
			title.setText(gift.getTitle());
		}
		if (gift.getDescription()!=null) {
			description.setText(gift.getDescription());
		}
		if (gift.getTouchCount()>0) {
			touchedCount.setText(""+gift.getTouchCount());
			touchedCount.setVisibility(View.VISIBLE);
			touchedCountImage.setVisibility(View.VISIBLE);
		} else {
			touchedCount.setVisibility(View.GONE);
			touchedCountImage.setVisibility(View.GONE);
		}
		if (gift.getFlagCount()>0) {
			flaggedCount.setText(""+gift.getFlagCount());
			flaggedCount.setVisibility(View.VISIBLE);
			flaggedCountImage.setVisibility(View.VISIBLE);
		} else {
			flaggedCount.setVisibility(View.GONE);
			flaggedCountImage.setVisibility(View.GONE);
		}

		User user=UserManager.getInstance().getUser();
		if (user!=null) {
			touchToggleButton.setChecked(false);
			flaggedToggleButton.setChecked(false);
			touchToggleButton.setEnabled(true);
			flaggedToggleButton.setEnabled(true);

			Log.v(TAG,"userId="+user.getId());
			for (GiftVote vote: gift.getTouchedUsers()) {
				Log.v(TAG,""+vote.getUserId());
				if (vote.getUserId()==user.getId()) {
					touchToggleButton.setChecked(true);
					break;
				}
			}
			for (GiftVote vote: gift.getFlaggedUsers()) {
				if (vote.getUserId()==user.getId()) {
					flaggedToggleButton.setChecked(true);
					break;
				}
			}
		} else {
			touchToggleButton.setEnabled(false);
			flaggedToggleButton.setEnabled(false);
		}
		
		return false;
	}
	
	static public boolean setImage(View view, Bitmap bitmap) {
		ImageView imageView=(ImageView) view.findViewById(R.id.imageView);
		int height=view.getHeight();
		Log.v(TAG,"view dimensions="+height);
		imageView.setMaxHeight((int) (height*.7));
		imageView.setMinimumHeight((int) (height*.2));
		if (bitmap==null) {
			//TODO Should set up image to get an image here, but...
			imageView.setVisibility(View.INVISIBLE);
			return false;
		} else {
			imageView.setVisibility(View.VISIBLE);
			imageView.setImageBitmap(bitmap);
			return true;
		}
	}

	private static class VoteOnClickListener implements OnClickListener {
		private Gift mGift;

		public VoteOnClickListener(Gift gift) {
			mGift=gift;
		}

		public void onClick(View v) {
			User user=UserManager.getInstance().getUser();
			ToggleButton tb=(ToggleButton) v;
			tb.toggle();
			Log.v(TAG,"isChecked="+tb.isChecked());
			if (user!=null) {
				VoteType type=null;
				switch(v.getId()) {
				case R.id.touchedButton: 
					if (tb.isChecked()) {
						type=VoteType.UNTOUCHED;
					} else {
						type=VoteType.TOUCHED;
					}
					break;
				case R.id.flagButton:
					if (tb.isChecked()) {
						type=VoteType.UNFLAGGED;
					} else {
						type=VoteType.FLAGGED;
					}
					break;
				}
				Intent intent=ThreadPoolCommandService.makeIntent(v.getContext(),
						new CommandHandler(v.getContext()),
						CommandHandler.GIFT_VOTE,
						new GiftCommandWrapper.VoteGift(mGift, type), null);
				v.getContext().startService(intent);
				tb.toggle();
			} else {
				new LoginDialogFragment().show(((Activity) v.getContext()).getFragmentManager(),"login");
			}
		}
	}

}
