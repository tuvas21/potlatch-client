package com.kd7uiy.potlatch.client.views;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.shared.GiftChain;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class GiftChainListViewHelper{
	
	@SuppressWarnings("unused")
	private static final String TAG = "smallGiftChainHelper";

	static public boolean populateInformation(View view, GiftChain chain) {
		//TODO- Add stuff here!
		TextView title=(TextView) view.findViewById(R.id.title);
		TextView touchedCount=(TextView) view.findViewById(R.id.touchedCount);
		TextView giftCount=(TextView) view.findViewById(R.id.giftCount);
		ImageView touchedIcon=(ImageView) view.findViewById(R.id.touchedIcon);
		ImageView giftIcon=(ImageView) view.findViewById(R.id.giftIcon);
		
		title.setText(chain.getName());
		
		if (chain.getTouchedCount()>0) {
			touchedCount.setVisibility(View.VISIBLE);
			touchedIcon.setVisibility(View.VISIBLE);
			touchedCount.setText(""+chain.getTouchedCount());
		} else {
			touchedCount.setVisibility(View.GONE);
			touchedIcon.setVisibility(View.GONE);
		}
		
		if (chain.getGiftCount()>0) {
			giftCount.setVisibility(View.VISIBLE);
			giftIcon.setVisibility(View.VISIBLE);
			giftCount.setText(""+chain.getGiftCount());
		} else {
			giftCount.setVisibility(View.GONE);
			giftIcon.setVisibility(View.GONE);
		}
		
		return true;
	}
	
	static public boolean setImage(View view, Bitmap bitmap) {
		ImageView imageView=(ImageView) view.findViewById(R.id.imageView);
		if (bitmap==null) {
			//Should set this to a image indicating that there isn't an image, but...
			imageView.setVisibility(View.INVISIBLE);
			return false;
		} else {
			imageView.setVisibility(View.VISIBLE);
			imageView.setImageBitmap(bitmap);
			return true;
		}
	}

}
