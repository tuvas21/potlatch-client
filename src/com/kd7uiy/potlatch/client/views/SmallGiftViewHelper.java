package com.kd7uiy.potlatch.client.views;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.shared.Gift;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SmallGiftViewHelper{
	
	@SuppressWarnings("unused")
	private static final String TAG = "smallGiftHelper";

	static public boolean populateGift(View view, Gift gift) {
		TextView title=(TextView) view.findViewById(R.id.title);
		TextView touchedCount=(TextView) view.findViewById(R.id.touchedCount);
		TextView flaggedCount=(TextView) view.findViewById(R.id.flagCount);
		ImageView touchedCountImage=(ImageView) view.findViewById(R.id.touchedIcon);
		ImageView flaggedCountImage=(ImageView) view.findViewById(R.id.flagIcon);
		ImageView chainedIconImage=(ImageView) view.findViewById(R.id.chainedIcon);
		
		if (gift.getTitle()!=null) {
			title.setText(gift.getTitle());
		}
		if (gift.getTouchCount()>0) {
			touchedCount.setText(""+gift.getTouchCount());
			touchedCount.setVisibility(View.VISIBLE);
			touchedCountImage.setVisibility(View.VISIBLE);
		} else {
			touchedCount.setVisibility(View.INVISIBLE);
			touchedCountImage.setVisibility(View.INVISIBLE);
		}
		if (gift.getFlagCount()>0) {
			flaggedCount.setText(""+gift.getFlagCount());
			flaggedCount.setVisibility(View.VISIBLE);
			flaggedCountImage.setVisibility(View.VISIBLE);
		} else {
			flaggedCount.setVisibility(View.INVISIBLE);
			flaggedCountImage.setVisibility(View.INVISIBLE);
		}
		
		if (gift.getChainId()>0) {
			chainedIconImage.setVisibility(View.VISIBLE);
		} else {
			chainedIconImage.setVisibility(View.INVISIBLE);
		}
		
		return false;
	}
	
	static public boolean setImage(View view, Bitmap bitmap) {
		ImageView imageView=(ImageView) view.findViewById(R.id.imageView);
		if (bitmap==null) {
			//Should set this to a image indicating that there isn't an image, but...
			imageView.setVisibility(View.INVISIBLE);
			return false;
		} else {
			imageView.setVisibility(View.VISIBLE);
			imageView.setImageBitmap(bitmap);
			return true;
		}
	}

}
