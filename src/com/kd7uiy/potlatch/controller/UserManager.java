package com.kd7uiy.potlatch.controller;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.ArrayList;

import com.kd7uiy.potlatch.client.CommandHandler;
import com.kd7uiy.potlatch.client.R;
import com.kd7uiy.potlatch.client.serverApi.GiftChainCommandWrapper;
import com.kd7uiy.potlatch.client.serverApi.UserCommandWrapper;
import com.kd7uiy.potlatch.shared.GiftChain;
import com.kd7uiy.potlatch.shared.User;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class UserManager extends BroadcastReceiver{

	private static final String TAG = "UserManager";
	private static UserManager instance;
	private User mUser;
	private ArrayList<GiftChain> mChains;

	private UserManager() {
	}
	
	public void registerBroadcastListener(LocalBroadcastManager lbm) {
		lbm.registerReceiver(this,new IntentFilter(CommandHandler.USER_INFO_UPDATED));
	}
	
	public synchronized static UserManager getInstance() {
		if (instance==null) {
			instance=new UserManager();
		}
		return instance;
	}
	
	public void setUser(User user) {
		mUser=user;
	}
	
	public User getUser() {
		return mUser;
	}
	
	public ArrayList<GiftChain> getGiftChains() {
		return mChains;
	}
	
	public boolean blockFlagged() {
		if (mUser!=null) {
			return mUser.getFilterOffensive();
		} else {
			return false;
		}
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		mUser= intent.getExtras().getParcelable(UserCommandWrapper.USER);
		if (mUser==null) {
			Toast.makeText(context,R.string.bad_password,Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(context,R.string.log_in_successful,Toast.LENGTH_LONG).show();
			Log.v(TAG,"Set current user to "+mUser);
			mChains=intent.getExtras().getParcelableArrayList(GiftChainCommandWrapper.GIFT_CHAIN_INFO);
		}
	}
}
