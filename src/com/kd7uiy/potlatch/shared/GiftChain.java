package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import android.os.Parcel;
import android.os.Parcelable;


public class GiftChain implements Parcelable{

	private long id;

	private String name;
	private long defaultImage=0;
	private String description;
	private long time;

	private int giftCount;
	private int touchedCount;
	private int flagCount;
	
	private long userId;
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public int getGiftCount() {
		return giftCount;
	}

	public void setGiftCount(int giftCount) {
		this.giftCount = giftCount;
	}

	public int getTouchedCount() {
		return touchedCount;
	}

	public void setTouchedCount(int touchedCount) {
		this.touchedCount = touchedCount;
	}

	public int getFlagCount() {
		return flagCount;
	}

	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public long getDefaultImage() {
		return defaultImage;
	}
	
	public void setDefaultImage(long defaultImage) {
		this.defaultImage = defaultImage;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time=time;
	}
	
	@Override
	public int describeContents() {
		return 2;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeLong(defaultImage);
		dest.writeLong(time);
	}
	
	public GiftChain(Parcel parcel) {
		id=parcel.readLong();
		name=parcel.readString();
		description=parcel.readString();
		defaultImage=parcel.readLong();
		time=parcel.readLong();
	}
	
	public GiftChain() {
	}

	public static final Parcelable.Creator<GiftChain> CREATOR
	= new Parcelable.Creator<GiftChain>() {
		public GiftChain createFromParcel(Parcel in) {
			return new GiftChain(in);
		}

		public GiftChain[] newArray(int size) {
			return new GiftChain[size];
		}
	};

}
