package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


public class Gift implements Parcelable{


	private long id;

	private String title;
	private long userId;
	private long chainId;
	private String description;
	private long time;
	
	private long touchCount;
	private long flagCount;
	
	List<GiftVote> touchedUsers;
	
	List<GiftVote> flaggedUsers;
	
	public Gift() {
	}
	
    public List<GiftVote> getTouchedUsers() {
        return touchedUsers;
    }

    public void setTouchedUsers(List<GiftVote> touchedUsers) {
        this.touchedUsers = touchedUsers;
    }
    
    public List<GiftVote> getFlaggedUsers() {
        return flaggedUsers;
    }

    public void setFlaggedUsers(List<GiftVote> flaggedUsers) {
        this.flaggedUsers = flaggedUsers;
    }

	public Gift(String title, long userId, long chainId, String description) {
		super();
		this.title = title;
		this.userId=userId;
		this.chainId=chainId;
		this.description=description;
	}

	
	public long getTouchCount() {
		return touchCount;
	}

	public void setTouchCount(long touchCount) {
		this.touchCount = touchCount;
	}
	
	public long getFlagCount() {
		return flagCount;
	}

	public void setFlagCount(long flagCount) {
		this.flagCount = flagCount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getChainId() {
		return chainId;
	}
	
	public void setChainId(long chainId) {
		this.chainId = chainId;
	}
	
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time=time;
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Gift object \n");
		sb.append("Id = " + id + "\n");
		sb.append("Title="+title+"\n");
		sb.append("Description="+description+"\n");
//		sb.append("User="+user.getName()+"\n");
//		sb.append("chain="+chain.getId()+"\n");
		return sb.toString();
	}
	
	@Override
	public int describeContents() {
		return 3;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeLong(time);
		dest.writeLong(touchCount);
		dest.writeLong(flagCount);
		dest.writeLong(userId);
		dest.writeLong(chainId);
	}
	
	public Gift(Parcel parcel) {
		id=parcel.readLong();
		title=parcel.readString();
		description=parcel.readString();
		time=parcel.readLong();
		touchCount=parcel.readLong();
		flagCount=parcel.readLong();
		userId=parcel.readLong();
		chainId=parcel.readLong();
	}
	
	public static final Parcelable.Creator<Gift> CREATOR
	= new Parcelable.Creator<Gift>() {
		public Gift createFromParcel(Parcel in) {
			return new Gift(in);
		}

		public Gift[] newArray(int size) {
			return new Gift[size];
		}
	};

}
