package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

public class GiftVote {
	

	private long id;

	private long userId;
	private long giftId;
	
	private long time;
	
	public enum VoteType {
		TOUCHED, FLAGGED
	}
	
	private VoteType type;
	
	public GiftVote() {
	}
	
	public GiftVote(User user, VoteType type, long id2) {
		this();
		this.userId=user.getId();
		this.type=type;
		this.giftId=id2;
		
		time=System.currentTimeMillis();
	}
	
	public long getTime() {
		return time;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id){
		this.id=id;
	}

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId=userId;
	}
	
	public long getGiftId() {
		return giftId;
	}
	
	public void setGiftId(long id){
		this.giftId=id;
	}
	
	public void setType(VoteType type) {
		this.type=type;
	}
	public VoteType getType() {
		return type;
	}
}
