package com.kd7uiy.potlatch.shared;

/* 
 *  Potlatch, Gift Sharing Program
	Copyright (c) 2015 http://www.kd7uiy.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable{

	private long id;

	private String name;
	private String openId;
	private String description;
	private boolean filterOffensive;
	private boolean pushNotifications;
	
	private int giftCount;
	private int touchCount;
	private int flagCount;

	private String password;
	
	public int getGiftCount() {
		return giftCount;
	}

	public void setGiftCount(int giftCount) {
		this.giftCount = giftCount;
	}

	public int getTouchCount() {
		return touchCount;
	}

	public void setTouchCount(int touchCount) {
		this.touchCount = touchCount;
	}

	public int getFlagCount() {
		return flagCount;
	}

	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}
	
	public User() {
		
	}
	
	public User(Parcel parcel) {
		id=parcel.readLong();
		name=parcel.readString();
		openId=parcel.readString();
		description=parcel.readString();
		filterOffensive=parcel.readByte()!=0;
		pushNotifications=parcel.readByte()!=0;
		password=parcel.readString();
	}
	
	public static User create(String username, String password, boolean filterOffensive) {
		User user=new User();
		user.openId=username;
		user.password=password;
		user.filterOffensive=filterOffensive;
		user.description="";
		user.pushNotifications=true;
		user.name="";
		return user;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	public boolean getFilterOffensive() {
		return filterOffensive;
	}
	public void setFilterOffensive(boolean filterOffensive) {
		this.filterOffensive=true;
	}
	
	public boolean getPushNotifications() {
		return pushNotifications;
	}
	public void setPushNotifications(boolean pushNotifications) {
		this.pushNotifications=true;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password=password;
	}

	public String getUsername() {
		return name;
	}
	
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("id="+id+"\n");
		sb.append("name="+name+"\n");
		sb.append("openId="+openId+"\n");
		sb.append("description="+description+"\n");
		sb.append("filterOffensive="+filterOffensive+"\n");
		sb.append("pushNotifications="+pushNotifications+"\n");
		sb.append("password="+password+"\n");
		return sb.toString();
	}

	@Override
	public int describeContents() {
		return 4;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(openId);
		dest.writeString(description);
		dest.writeByte((byte) (filterOffensive ? 1 : 0));
		dest.writeByte((byte) (pushNotifications ? 1 : 0));
		dest.writeString(password);
	}
	
	public static final Parcelable.Creator<User> CREATOR
	= new Parcelable.Creator<User>() {
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		public User[] newArray(int size) {
			return new User[size];
		}
	};

}
